import logo from './images/logo.png';
import slide1 from './images/slide1.jpg';
import slide2 from './images/slide2.jpg';
import slide3 from './images/slide3.jpg';
import minus from './icons/minus.svg';
import plus from './icons/plus.svg';
import close from './icons/close.svg';
import spinner from './icons/spinner.svg';
import favicon from './icons/favicon.png';

export { logo, slide1, slide2, slide3, minus, plus, close, spinner, favicon };
