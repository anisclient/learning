import React from 'react';
import ReactDom from 'react-dom';
import App from './app';
import i18next from 'i18next';
// import { i18nextReactInitConfig } from '@ijl/cli';
//////////////////////////
import axios from 'axios';
import { initReactI18next } from 'react-i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
/*global module, __webpack_public_path__*/
/*eslint no-undef: "error"*/
const loadPath = () => `${__webpack_public_path__}locales/{{lng}}.json`;

const i18nextAxios = axios.create();

const ajax = (url, opts, callback) =>
    i18nextAxios(url).then((response) => {
        const enrichedLocales = {
            ...response.data,
        };
        callback(JSON.stringify(enrichedLocales), response);
    });

// export const i18nextInitConfig = (i18next) =>
//     i18next.use(XHR).init({
//         lng: localStorage.getItem('i18nextLng') || 'ru',
//         fallbackLng: 'ru',
//         keySeparator: false,
//         backend: {
//             loadPath,
//             ajax,
//         },
//     });

export const i18nextReactInitConfig = (i18next) =>
    i18next
        .use(XHR)
        .use(initReactI18next)
        .use(LanguageDetector)
        .init({
            fallbackLng: 'ru',
            load: 'currentOnly',
            keySeparator: false,
            whitelist: ['ru', 'en'],
            backend: {
                loadPath,
                ajax,
            },
        });

///////////////////////////

// i18next.t = i18next.t.bind(i18next);
const i18newNext = i18nextReactInitConfig(i18next);

export default (): React.ReactNode => <App />;

export const mount = async (Сomponent: React.FC) => {
    await Promise.all([i18newNext]);
    ReactDom.render(<Сomponent />, document.getElementById('app'));

    if (module.hot) {
        module.hot.accept('./app', () => {
            // i18next.reloadResources();
            ReactDom.render(<App />, document.getElementById('app'));
        });
    }
};

export const unmount = (): void => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};
