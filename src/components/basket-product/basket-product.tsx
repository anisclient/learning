import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import style from './style.css';
import { URLs } from '../../__data__/urls';
import BasketButton, {
    BasketButtonTypes,
} from '../../components/basket-button';
import { useDispatch } from 'react-redux';
// import { chooseProductToSingleProductPage } from '../../__data__/actions/products';
import {
    AllProducts,
    deleteProductsGroup,
    minusProductById,
    plusProductById,
} from '../../__data__/store/reducers/basket';
import { ProductsItem } from '../../__data__/store/reducers/products';
import PropTypes from 'prop-types';
import ModalWindow, { ModalWindowType } from '../modal-window';
import { useTranslation } from 'react-i18next';

interface BasketProductProps {
    product: ProductsItem;
    allProducts: AllProducts | AllProducts[];
}

const BasketProduct: React.FC<BasketProductProps> = ({
    product,
    allProducts,
}) => {
    const dispatch = useDispatch();
    const { t } = useTranslation();

    const [modalWindowActive, changeModalWindowActive] = useState(false);

    // function chooseProduct() {
    //     dispatch(chooseProductToSingleProductPage(product));
    // }

    function minusProduct() {
        dispatch(minusProductById(product.id));
    }

    function plusProduct() {
        dispatch(plusProductById(product.id));
    }

    function showPopapDelete() {
        changeModalWindowActive(!modalWindowActive);
    }

    function deleteProduct() {
        showPopapDelete();
        dispatch(deleteProductsGroup(product.id));
    }

    return (
        <div className={style.basketProductsCard}>
            <Link
                to={`${URLs.product.url}/${product.id}`}
                className={style.basketProductsView}
                // onClick={chooseProduct}
            >
                <div className={style.basketProductsImg}>
                    <img
                        src={product.image}
                        alt={
                            localStorage.getItem('i18nextLng') === 'ru'
                                ? product.title.ru
                                : product.title.en
                        }
                    />
                </div>
                <div className={style.basketProductsDescription}>
                    {localStorage.getItem('i18nextLng') === 'ru'
                        ? product.title.ru
                        : product.title.en}
                </div>
            </Link>

            {modalWindowActive && (
                <ModalWindow
                    buttonText={t('i18next.basketproduct.delete')}
                    modalText={t(
                        'i18next.basketproduct.questionformodalwindow'
                    )}
                    switchShowPopap={showPopapDelete}
                    handler={deleteProduct}
                    type={ModalWindowType.forBasket}
                />
            )}

            <div className={style.basketProductsCounting}>
                <div className={style.basketProductsCount}>
                    <BasketButton
                        buttonType={BasketButtonTypes.minus}
                        onClick={minusProduct}
                    />
                    <strong>
                        {allProducts[product.id].productsById.length}
                    </strong>
                    <BasketButton
                        buttonType={BasketButtonTypes.plus}
                        onClick={plusProduct}
                    />
                </div>
                <p className={style.basketProductsPrice}>
                    <strong>
                        {allProducts[product.id].totalPriceForThisGroup}
                    </strong>
                    <small>₽</small>
                </p>
                <BasketButton
                    buttonType={BasketButtonTypes.close}
                    onClick={showPopapDelete}
                />
            </div>
        </div>
    );
};

BasketProduct.propTypes = {
    product: PropTypes.exact({
        title: PropTypes.exact({
            ru: PropTypes.string.isRequired,
            en: PropTypes.string.isRequired,
        }).isRequired,
        price: PropTypes.number.isRequired,
        rating: PropTypes.number.isRequired,
        image: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        liked: PropTypes.bool.isRequired,
    }).isRequired,
    // allProducts: PropTypes.oneOfType([
    //     PropTypes.exact({
    //         productsById: PropTypes.array,
    //         totalPriceForThisGroup: PropTypes.number,
    //     }),
    //     PropTypes.arrayOf(
    //         PropTypes.exact({
    //             productsById: PropTypes.array,
    //             totalPriceForThisGroup: PropTypes.number,
    //         })
    //     ),
    // ]).isRequired,
};

export default BasketProduct;
