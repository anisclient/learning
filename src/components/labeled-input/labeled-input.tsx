import React from 'react';
import PropTypes from 'prop-types';
import style from './style.css';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import { FieldRenderProps } from 'react-final-form';

interface LinkType {
    to: string;
    text: string;
}

interface LabeledInputProps
    extends Omit<
        React.DetailedHTMLProps<
            React.InputHTMLAttributes<HTMLInputElement>,
            HTMLInputElement
        >,
        'id'
    > {
    id: string | number;
    name?: string;
    label: string;
    placeholder: string;
    type?: string;
    inputRef?: React.RefObject<HTMLInputElement>;
    link?: LinkType;
    error?: string | boolean;
}

export const LabeledInput: React.FC<
    Partial<FieldRenderProps<LabeledInputProps>>
> = ({
    id,
    name,
    inputRef,
    type,
    label,
    placeholder,
    link,
    error,
    input,
    meta,
    ...rest
}) => {
    return (
        <div className={style.wrapper}>
            <label htmlFor={String(id)} className={style.label}>
                {label}
            </label>
            {((error && typeof error !== 'boolean') ||
                ((meta?.error ||
                    (meta?.submitError && !meta?.modifiedSinceLastSubmit)) &&
                    meta?.modified)) && (
                <div className={style.error}>
                    {meta?.error || meta?.submitError || error}
                </div>
            )}
            <input
                {...input}
                value={String(input?.value) || rest.value || ''}
                placeholder={placeholder}
                ref={inputRef}
                type={input?.type || type}
                name={name}
                id={String(id)}
                className={cn(
                    style.input,
                    (((meta?.error ||
                        (meta?.submitError &&
                            !meta?.modifiedSinceLastSubmit)) &&
                        meta?.modified) ||
                        error) &&
                        style.danger
                )}
                {...rest}
            />
            {link && (
                <Link to={link.to} className={style.link}>
                    {link.text}
                </Link>
            )}
        </div>
    );
};

LabeledInput.propTypes = {
    // placeholder: (props, propName) => {
    //     const value = props[propName];
    //     const length = value.split(/\s/).length;
    //     if (length < 2 || length > 3) {
    //         return new Error(`Длина поля ${propName} не равно 2-ум или 3-м`);
    //     }
    // },
    placeholder: PropTypes.string,
    label: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
    type: PropTypes.oneOf(['text', 'password', 'number']),
    inputRef: PropTypes.exact({
        current: PropTypes.instanceOf(HTMLInputElement),
    }),
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    link: PropTypes.exact({
        to: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
    }),
};

LabeledInput.defaultProps = {
    type: 'text',
};
