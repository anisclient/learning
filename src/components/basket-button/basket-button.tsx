import React from 'react';
import style from './style.css';
import cn from 'classnames';
import { BasketButtonTypes } from './model';
import PropTypes from 'prop-types';

interface BasketButtonProps
    extends React.DetailedHTMLProps<
        React.ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
    > {
    buttonType: BasketButtonTypes;
    onClick?: () => void;
    className?: string;
}

const BasketButton: React.FC<BasketButtonProps> = ({
    buttonType,
    className,
    onClick,
    ...rest
}) => {
    return (
        <button
            {...rest}
            onClick={onClick}
            className={cn(style.main, style[buttonType], className)}
        ></button>
    );
};

BasketButton.propTypes = {
    buttonType: PropTypes.oneOf([
        BasketButtonTypes.minus,
        BasketButtonTypes.plus,
        BasketButtonTypes.close,
    ]).isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

export default BasketButton;
