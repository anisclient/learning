/* eslint-disable */
export enum BasketButtonTypes {
    minus = 'basket-products-minus',
    plus = 'basket-products-plus',
    close = 'basket-products-close',
}
