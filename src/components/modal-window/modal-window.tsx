import React from 'react';
import style from './style.css';
import Button, { ButtonColors } from '../button';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

export enum ModalWindowType {
    onlyInformationType,
    forBasket,
}

interface ModalWindowProps {
    modalText: string;
    buttonText: string;
    switchShowPopap: () => void;
    handler: () => void;
    type: ModalWindowType;
}

const ModalWindow: React.FC<ModalWindowProps> = ({
    modalText,
    buttonText,
    switchShowPopap,
    handler,
    type,
}) => {
    const { t } = useTranslation();
    function commitHandler() {
        switchShowPopap();
        handler();
    }

    return (
        <div>
            <div className={style.modal}>
                <p className={style.modalText}>{modalText}</p>
                <Button
                    onClick={commitHandler}
                    colorScheme={ButtonColors.blue}
                    className={style.buttonOffset}
                >
                    {buttonText}
                </Button>
                {type === ModalWindowType.forBasket && (
                    <Button
                        colorScheme={ButtonColors.lightgreen}
                        className={style.buttonOffset}
                        onClick={switchShowPopap}
                    >
                        {t('i18next.modalwindow.cancel')}
                    </Button>
                )}
            </div>
            <div className={style.overlay}></div>
        </div>
    );
};

export default ModalWindow;

ModalWindow.propTypes = {
    modalText: PropTypes.string.isRequired,
    buttonText: PropTypes.string.isRequired,
    switchShowPopap: PropTypes.func.isRequired,
    handler: PropTypes.func.isRequired,
    type: PropTypes.oneOf([
        ModalWindowType.onlyInformationType,
        ModalWindowType.forBasket,
    ]).isRequired,
};
