import { ModalWindowType } from './modal-window';

export { default } from './modal-window';

export { ModalWindowType };
