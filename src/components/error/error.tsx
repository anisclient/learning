import React from 'react';
import style from './style.css';
import PropTypes from 'prop-types';

type ErrorProps = {
    error?: {
        text: string;
        title?: string;
    };
};

const Error: React.FC<ErrorProps> = ({ error: { text, title } }) => (
    <div className={style.wrapper}>
        {title && <h2 className={style.title}>{title}</h2>}
        <p className={style.errorText}>{text}</p>
    </div>
);

Error.propTypes = {
    error: PropTypes.exact({
        text: PropTypes.string.isRequired,
        title: PropTypes.string,
    }),
};

Error.defaultProps = {
    error: {
        text: 'Извините, что-то пошло не так',
    },
};

export default Error;
