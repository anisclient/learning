import React from 'react';
import Error from '../error';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
    state = {
        isError: false,
    };

    static propTypes = {
        children: PropTypes.node.isRequired,
    };

    static getDerivedStateFromError(): { isError: boolean } {
        return {
            isError: true,
        };
    }

    render(): React.ReactNode {
        const { isError } = this.state;

        if (isError) {
            return <Error />;
        }

        return this.props.children;
    }
}

export default ErrorBoundary;
