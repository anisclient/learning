import React from 'react';
import style from './style.css';
import PropTypes from 'prop-types';
import cn from 'classnames';

interface IconProps {
    text: string;
    activeIcon: boolean;
    children: React.ReactNode;
}

const Icon: React.FC<IconProps> = ({ text, activeIcon, children }) => {
    return (
        <div className={cn(style.icon, activeIcon && style.active)}>
            {children}
            <span>{text}</span>
        </div>
    );
};

Icon.propTypes = {
    text: PropTypes.string.isRequired,
    activeIcon: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
};

export default Icon;
