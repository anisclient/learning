import React from 'react';
import style from './style.css';
import { FieldRenderProps } from 'react-final-form';
import cn from 'classnames';

type Props = FieldRenderProps<string, any>;

export const TextArea: React.FC<Props> = ({ input, meta, ...rest }: Props) => (
    <div className={style.wrapper}>
        {(meta?.error ||
            (meta?.submitError && !meta?.modifiedSinceLastSubmit)) &&
            meta?.modified && (
                <div className={style.error}>
                    {meta?.error || meta?.submitError}
                </div>
            )}
        <textarea
            className={cn(
                style.textarea,
                (meta?.error ||
                    (meta?.submitError && !meta?.modifiedSinceLastSubmit)) &&
                    meta?.modified &&
                    style.danger
            )}
            {...input}
            {...rest}
        />
    </div>
);
