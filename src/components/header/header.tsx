import React, { useEffect } from 'react';
import style from './style.css';
import { Logo, Navigation, BurgerBtn } from '../index';
import { Item } from '../navigation';
// import PersonIcon from './person-icon';
import LikeIcon from './like-icon';
import BasketIcon from './basket-icon';
import PropTypes from 'prop-types';
import { URLs } from '../../__data__/urls';
import { useTranslation } from 'react-i18next';
import I18nbuttons from '../i18nbuttons';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../__data__/selectors';
import { setProducts } from '../../__data__/actions/products';

interface HeaderProps {
    active: boolean;
    setActive: React.Dispatch<React.SetStateAction<boolean>>;
}

const Header: React.FC<HeaderProps> = ({ active, setActive }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const productsInMain = useSelector(selectors.allProducts.productsInMain);

    useEffect(() => {
        if (productsInMain && productsInMain.length === 0) {
            dispatch(setProducts());
        }
    }, [productsInMain]);

    const itemsToNavigation: Item[] = [
        // {
        //     id: 1,
        //     text: isLogin ? 'Выйти' : 'Войти',
        //     to: URLs.login.url,
        //     icon: <PersonIcon />,
        // },
        {
            id: 2,
            text: t('i18next.header.favorites'),
            to: URLs.like.url,
            icon: <LikeIcon />,
        },
        {
            id: 3,
            text: t('i18next.header.basket'),
            to: URLs.basket.url,
            icon: <BasketIcon />,
        },
    ];

    return (
        <header className={style.header}>
            <div className={style.container}>
                <div className={style.headerBody}>
                    <Logo setActive={setActive} />
                    <I18nbuttons />
                    <BurgerBtn active={active} setActive={setActive} />
                    <Navigation
                        active={active}
                        setActive={setActive}
                        items={itemsToNavigation}
                    />
                </div>
            </div>
        </header>
    );
};

Header.propTypes = {
    active: PropTypes.bool.isRequired,
    setActive: PropTypes.func.isRequired,
};

export default Header;
