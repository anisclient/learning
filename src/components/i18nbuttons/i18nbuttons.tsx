import React, { useEffect, useState } from 'react';
import style from './style.css';
import { useTranslation } from 'react-i18next';

const I18nbuttons: React.FC = () => {
    const { i18n } = useTranslation();

    const [active, setActive] = useState('');

    useEffect(() => {
        setActive(localStorage.getItem('i18nextLng'));
    }, []);

    const handleChangeLangToRu = async () => {
        setActive('ru');
        await i18n.changeLanguage('ru');
    };

    const handleChangeLangToEn = async () => {
        setActive('en');
        await i18n.changeLanguage('en');
    };

    return (
        <div className={style.i18ncontainer}>
            <button
                className={active === 'ru' ? style.active : ''}
                onClick={handleChangeLangToRu}
            >
                Ru
            </button>
            <button
                className={active === 'en' ? style.active : ''}
                onClick={handleChangeLangToEn}
            >
                En
            </button>
        </div>
    );
};

export default I18nbuttons;
