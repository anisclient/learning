import React, { Suspense } from 'react';
import ErrorBoundary from '../error-boundary';
import PropTypes from 'prop-types';

interface LazyComponentProps {
    children: React.ReactNode;
    fallback?: React.ReactNode;
}

export const LazyComponent: React.FC<LazyComponentProps> = ({
    children,
    fallback,
}) => {
    return (
        <ErrorBoundary>
            <Suspense fallback={fallback}>{children}</Suspense>
        </ErrorBoundary>
    );
};

LazyComponent.propTypes = {
    children: PropTypes.node.isRequired,
    fallback: PropTypes.node,
};

LazyComponent.defaultProps = {
    fallback: <div>Loading...</div>,
};
