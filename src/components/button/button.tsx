import React, { DetailedHTMLProps, ButtonHTMLAttributes } from 'react';
import style from './style.css';
import cn from 'classnames';
import { ButtonColors } from './model';
import PropTypes from 'prop-types';
import { spinner } from '../../assets';
/* eslint-disable no-unused-vars */

interface ButtonProps
    extends DetailedHTMLProps<
        ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
    > {
    colorScheme: ButtonColors;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    className?: string;
    loading?: boolean;
}

const Button: React.FC<ButtonProps> = ({
    colorScheme,
    children,
    onClick,
    className,
    loading,
    ...rest
}) => {
    return (
        <button
            {...rest}
            className={cn(style.main, style[colorScheme], className)}
            onClick={onClick}
        >
            {loading ? (
                <span className={style.spinner}>
                    <img src={spinner} alt="loading" />
                </span>
            ) : (
                children
            )}
        </button>
    );
};

Button.propTypes = {
    colorScheme: PropTypes.oneOf([
        ButtonColors.green,
        ButtonColors.blue,
        ButtonColors.transparent,
        ButtonColors.lightgreen,
    ]).isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
    loading: PropTypes.bool,
};

export default Button;
