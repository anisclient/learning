import style from './style.css';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import LikeIcon from '../header/like-icon';
import Star from './star';
import Button, { ButtonColors } from '../button';
import { URLs } from '../../__data__/urls';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { ProductsItem } from '../../__data__/store/reducers/products';
import {
    addToBasket as addToBasketReducer,
    AllProducts,
} from '../../__data__/store/reducers/basket';
import { useDispatch, useSelector } from 'react-redux';
import { likeProduct, unlikeProduct } from '../../__data__/actions/products';
// import { chooseProductToSingleProductPage } from '../../__data__/actions/products';
import { AppStore } from '../../__data__/store/reducers';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

export enum ProductViewType {
    forMain = 'product-container',
    forLike = 'like-container',
}

interface ProductsProp {
    product: ProductsItem;
    type: ProductViewType;
}

const Product: React.FC<ProductsProp> = ({ product, type }) => {
    const { title, price, rating, image, liked, id } = product;
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const [like, setLike] = useState(false);

    const allProductsFromBasket = useSelector<
        AppStore,
        AllProducts | AllProducts[]
    >(({ basket }) => basket.allProducts);

    useEffect(() => {
        setLike(liked);
    }, [liked]);

    function handleLike(e) {
        dispatch(likeProduct(product));
        e.preventDefault();
    }

    function handleUnlike(e) {
        dispatch(unlikeProduct(product));
        e.preventDefault();
    }

    // function chooseProduct() {
    //     dispatch(chooseProductToSingleProductPage(product));
    // }

    function addToBasket(e) {
        dispatch(addToBasketReducer(product));
        e.preventDefault();
    }

    return (
        <div className={style[type]}>
            <Link
                to={`${URLs.product.url}/${id}`}
                className={style.product}
                // onClick={chooseProduct}
            >
                {like ? (
                    <div
                        className={cn(style.productLike, style.productActive)}
                        title={t('i18next.product.removefromfavorites')}
                    >
                        <button
                            className={style.likeButton}
                            onClick={(e) => handleUnlike(e)}
                        >
                            <LikeIcon />
                        </button>
                    </div>
                ) : (
                    <div
                        className={style.productLike}
                        title={t('i18next.product.addtofavorites')}
                    >
                        <button
                            className={style.likeButton}
                            onClick={(e) => handleLike(e)}
                        >
                            <LikeIcon />
                        </button>
                    </div>
                )}
                <div className={style.productImg}>
                    <img
                        src={image}
                        alt={
                            localStorage.getItem('i18nextLng') === 'ru'
                                ? title.ru
                                : title.en
                        }
                    />
                </div>
                <p className={style.productPrice}>
                    <strong>{price}</strong>
                    <small>₽</small>
                </p>
                <p className={style.productText}>
                    {localStorage.getItem('i18nextLng') === 'ru'
                        ? title.ru
                        : title.en}
                </p>
                <div className={style.productRating}>
                    {Array(rating)
                        .fill(rating)
                        .map((_, i) => (
                            <span role="img" aria-label="star" key={i}>
                                <Star />
                            </span>
                        ))}
                </div>
                <Button
                    colorScheme={ButtonColors.green}
                    className={style.buttonOffset}
                    onClick={(e) => addToBasket(e)}
                >
                    {allProductsFromBasket[id]
                        ? `${t('i18next.button.added')}:
                              ${
                                  allProductsFromBasket[id] &&
                                  allProductsFromBasket[id].productsById.length
                              } ${
                              allProductsFromBasket[id] &&
                              allProductsFromBasket[id].productsById.length ===
                                  1
                                  ? t('i18next.button.pc')
                                  : t('i18next.button.pcs')
                          }.`
                        : t('i18next.button.addtocart')}
                </Button>
            </Link>
        </div>
    );
};

Product.propTypes = {
    product: PropTypes.exact({
        title: PropTypes.exact({
            ru: PropTypes.string.isRequired,
            en: PropTypes.string.isRequired,
        }).isRequired,
        price: PropTypes.number.isRequired,
        rating: PropTypes.number.isRequired,
        image: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        liked: PropTypes.bool.isRequired,
    }).isRequired,
    type: PropTypes.oneOf([ProductViewType.forLike, ProductViewType.forMain])
        .isRequired,
};

export default Product;
