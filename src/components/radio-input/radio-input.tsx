import React from 'react';
import style from './style.css';
import { FieldRenderProps } from 'react-final-form';

export function RadioInput<T extends string>({
    id,
    input,
    children,
    ...rest
}: FieldRenderProps<T, any>) {
    return (
        <label className={style.label}>
            <input
                id={String(id)}
                className={style.input}
                type="radio"
                {...input}
                {...rest}
            />
            <div className={style.text}>{children}</div>
        </label>
    );
}
