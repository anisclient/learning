import React, { useEffect, useState } from 'react';
import style from './style.css';
import { Link, useLocation } from 'react-router-dom';
import { Icon } from '../index';
import cn from 'classnames';
import PropTypes from 'prop-types';
import PersonIcon from '../header/person-icon';
import { URLs } from '../../__data__/urls';
import { useSelector, useDispatch } from 'react-redux';
import * as selectors from '../../__data__/selectors';
import { setIsLogin } from '../../__data__/store/slices/user';
import { useTranslation } from 'react-i18next';

export interface Item {
    id: number;
    text: 'Войти' | 'Выйти' | 'Избранное' | 'Корзина';
    to: '/e_zone/login' | '/e_zone/like' | '/e_zone/basket';
    icon: React.ReactNode;
}

interface NavigationProps {
    items: Array<Item>;
    active: boolean;
    setActive: React.Dispatch<React.SetStateAction<boolean>>;
}

const Navigation: React.FC<NavigationProps> = ({
    items,
    active,
    setActive,
}) => {
    const { t } = useTranslation();
    const isLogin = useSelector(selectors.user.isLogin);
    const dispatch = useDispatch();

    function logout() {
        dispatch(setIsLogin(false));
        window.localStorage.removeItem('login');
        // dispatch(resetLoginActionCreator())
    }

    const location = useLocation();

    const arrLocation = location.pathname.split('/');

    const lastItem = arrLocation[arrLocation.length - 1];

    const [activeIcon, setActiveIcon] = useState('');

    useEffect(() => {
        switch (lastItem) {
            case 'login':
                setActiveIcon('1');
                break;
            case 'like':
                setActiveIcon('2');
                break;
            case 'basket':
                setActiveIcon('3');
                break;
            default:
                setActiveIcon('');
                break;
        }
    }, [location]);

    function handleActiveWithLogout() {
        setActive(false);
        isLogin && logout();
    }

    function handleActive() {
        setActive(false);
    }

    return (
        <nav className={cn(style.nav, active ? style.active : '')}>
            <ul className={style.navList}>
                <li key="1" className={style.navItem}>
                    <Link
                        to={URLs.login.url}
                        className={style.navLink}
                        onClick={handleActiveWithLogout}
                    >
                        <Icon
                            text={
                                isLogin
                                    ? t('i18next.header.logout')
                                    : t('i18next.header.login')
                            }
                            activeIcon={activeIcon === '1' ? true : false}
                        >
                            <PersonIcon />
                        </Icon>
                    </Link>
                </li>

                {items.map((item) => (
                    <li key={item.id} className={style.navItem}>
                        <Link
                            to={item.to}
                            className={style.navLink}
                            onClick={handleActive}
                        >
                            <Icon
                                text={item.text}
                                activeIcon={
                                    activeIcon === `${item.id}` ? true : false
                                }
                            >
                                {item.icon}
                            </Icon>
                        </Link>
                    </li>
                ))}
            </ul>
        </nav>
    );
};

Navigation.propTypes = {
    items: PropTypes.array.isRequired,
    active: PropTypes.bool.isRequired,
    setActive: PropTypes.func.isRequired,
};

export default Navigation;
