import React, { useState } from 'react';
import { mount } from 'enzyme';
import { describe, it, expect } from '@jest/globals';
import Header from '../header';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

describe('<Header />', () => {
    it('Отрисовывается без ошибок', () => {
        const Test = () => {
            const [burgerActive, setBurgerActive] = useState(false);
            return (
                <Provider store={store}>
                    <Header active={burgerActive} setActive={setBurgerActive} />
                </Provider>
            );
        };
        // expect.assertions(1);
        const wrapper = mount(<Test />);

        expect(wrapper).toMatchSnapshot();
    });
});
