import React, { useState } from 'react';
import { mount } from 'enzyme';
import { describe, it, expect } from '@jest/globals';
import Navigation from '../navigation/navigation';
import { store } from '../../__data__/store';
import { Provider } from 'react-redux';
import { Item } from '../navigation';
// import LikeIcon from './like-icon';
// import BasketIcon from './basket-icon';

const itemsToNavigation: Item[] = [
    // {
    //     id: 1,
    //     text: isLogin ? 'Выйти' : 'Войти',
    //     to: URLs.login.url,
    //     icon: <PersonIcon />,
    // },
    {
        id: 2,
        text: 'Избранное',
        to: '/e_zone/like',
        icon: 'Like',
    },
    {
        id: 3,
        text: 'Корзина',
        to: '/e_zone/basket',
        icon: 'Cart',
    },
];

describe('<I18nbuttons />', () => {
    it('Отрисовывается без ошибок', () => {
        const Test = () => {
            const [active, setActive] = useState(false);
            return (
                <Provider store={store}>
                    <Navigation
                        active={active}
                        setActive={setActive}
                        items={itemsToNavigation}
                    />
                </Provider>
            );
        };
        // expect.assertions(3);
        const wrapper = mount(<Test />);

        expect(wrapper).toMatchSnapshot();

        //click to login
        wrapper.find('a').at(0).simulate('click');
        wrapper.update();
        expect(wrapper).toMatchSnapshot();

        //click to like
        wrapper.find('a').at(1).simulate('click');
        wrapper.update();
        expect(wrapper).toMatchSnapshot();

        //click to basket
        wrapper.find('a').at(2).simulate('click');
        wrapper.update();
        expect(wrapper).toMatchSnapshot();
    });
});
