import React from 'react';
import { mount } from 'enzyme';
import { describe, it, expect } from '@jest/globals';
import Button, { ButtonColors } from '../button';

describe('<Button />', () => {
    it('Отрисовывается без ошибок', () => {
        expect.assertions(1);
        const wrapper = mount(<Button colorScheme={ButtonColors.blue} />);

        expect(wrapper).toMatchSnapshot();
    });
});
