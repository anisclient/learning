import React from 'react';
import { mount } from 'enzyme';
import { describe, it, expect } from '@jest/globals';
import I18nbuttons from '../i18nbuttons';

describe('<I18nbuttons />', () => {
    it('Отрисовывается без ошибок', () => {
        expect.assertions(3);
        const wrapper = mount(<I18nbuttons />);

        expect(wrapper).toMatchSnapshot();

        //click to Ru
        wrapper.find('button').first().simulate('click');
        wrapper.update();
        expect(wrapper).toMatchSnapshot();

        //click to En
        wrapper.find('button').last().simulate('click');
        wrapper.update();
        expect(wrapper).toMatchSnapshot();
    });
});
