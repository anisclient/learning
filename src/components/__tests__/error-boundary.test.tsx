import React from 'react';
import { mount } from 'enzyme';
import { describe, it, expect } from '@jest/globals';
import ErrorBoundary from '../error-boundary/index';

describe('Testing ErrorBoundary', () => {
    const Test = () => <div>Test</div>;
    const myError = new Error('Super special Error');
    it('Render ErrorBoundary', () => {
        const component = mount(
            <ErrorBoundary>
                <Test />
            </ErrorBoundary>
        );
        expect(component).toMatchSnapshot();

        component.find(Test).simulateError(myError);
        expect(component.state()).toHaveProperty('isError', true);
        expect(component).toMatchSnapshot();
    });
});
