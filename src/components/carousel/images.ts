/* eslint-disable */
import { slide1 } from '../../assets';
import { slide2 } from '../../assets';
import { slide3 } from '../../assets';

function A(image: any): any {
    return image;
}

export enum Images {
    Slide1 = A(slide1),
    Slide2 = A(slide2),
    Slide3 = A(slide3),
}
