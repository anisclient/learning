import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import { Images } from './index';
import style from './style.css';
import PropTypes from 'prop-types';

export interface CarouselSlideItem {
    slide: Images;
    id: number;
    title: string;
    link: string;
}

interface CarouselProps {
    slides: CarouselSlideItem[];
}

const Carousel: React.FC<CarouselProps> = ({ slides }) => {
    const [images, setImages] = useState([]);

    useEffect(() => {
        setImages(slides);
    }, []);

    const settings = {
        autoplay: true,
        infinite: true,
        autoplaySpeed: 3000,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
    };

    return (
        <div className={style.carousel}>
            {images.length === 0 ? (
                <div className={style.loading}>
                    <span>Loading...</span>
                </div>
            ) : (
                <Slider {...settings}>
                    {images.map((image) => (
                        <a
                            href={image.link}
                            target="_blank"
                            rel="noreferrer"
                            key={image.id}
                            className={style.item}
                        >
                            <img
                                src={image.slide}
                                alt={image.title}
                                className={style.img}
                            />
                        </a>
                    ))}
                </Slider>
            )}
        </div>
    );
};

// Carousel.propTypes = {
//     slides: PropTypes.arrayOf(
//         PropTypes.shape({
//             slide: PropTypes.oneOf<Images>([
//                 Images.Slide1,
//                 Images.Slide2,
//                 Images.Slide3,
//             ]).isRequired,
//             id: PropTypes.number.isRequired,
//             title: PropTypes.string.isRequired,
//             link: PropTypes.string.isRequired,
//         }).isRequired
//     ).isRequired,
// };

Carousel.propTypes = {
    slides: PropTypes.array.isRequired,
};

export default Carousel;
