import React from 'react';
import style from './style.css';
import cn from 'classnames';
import PropTypes from 'prop-types';

interface BurgerBtnProps {
    active: boolean;
    setActive: React.Dispatch<React.SetStateAction<boolean>>;
}

const BurgerBtn: React.FC<BurgerBtnProps> = ({ active, setActive }) => {
    function handleActive() {
        setActive(!active);
    }

    return (
        <div
            className={cn(style.burger, active ? style.active : '')}
            onClick={handleActive}
        >
            <span></span>
        </div>
    );
};

BurgerBtn.propTypes = {
    active: PropTypes.bool.isRequired,
    setActive: PropTypes.func.isRequired,
};

export default BurgerBtn;
