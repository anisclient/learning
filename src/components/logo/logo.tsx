import React from 'react';
import style from './style.css';
import { logo } from '../../assets';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { URLs } from '../../__data__/urls';

interface LogoProps {
    setActive: React.Dispatch<React.SetStateAction<boolean>>;
}

const Logo: React.FC<LogoProps> = ({ setActive }) => {
    function handleActive() {
        setActive(false);
    }

    return (
        <Link
            to={URLs.main.url}
            className={style.logoLink}
            onClick={handleActive}
        >
            <div className={style.logo}>
                <img src={logo} alt="e_zone" />
            </div>
        </Link>
    );
};

Logo.propTypes = {
    setActive: PropTypes.func.isRequired,
};

export default Logo;
