import { LabeledInput, Button, ButtonColors } from '../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { Link, Redirect } from 'react-router-dom';
import { URLs } from '../../__data__/urls';
import { useDispatch, useSelector } from 'react-redux';
import {
    // loginChangeActionCreator as setLogin,
    // passwordChangeActionCreator as setPassword,
    // setErrorActionCreator as setError,
    submitLoginFormActionCreator as submit,
} from '../../__data__/actions/login';
// import PropTypes from 'prop-types';
import * as selectors from '../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

interface Values {
    login: string;
    password: string;
}

// interface MapStateToProps {
//     login: string;
//     password: string;
//     error: string | boolean;
//     isReady: boolean;
//     loading: boolean;
// }

// interface MapDispatchToProps {
//     setLogin: (value: string) => void;
//     setPassword: (value: string) => void;
//     setError: (error: string | boolean) => void;
//     submit: (data: Data) => void;
// }

// type LoginProps = MapStateToProps & MapDispatchToProps;

const Login: React.FC = () =>
    //     {
    //     login,
    //     password,
    //     error,
    //     isReady,
    //     setLogin,
    //     setPassword,
    //     setError,
    //     loading,
    //     submit,
    // }
    {
        const { t } = useTranslation();

        const loginRef = useRef<HTMLInputElement>();

        // const login = useSelector(selectors.login.login);
        // const password = useSelector(selectors.login.password);
        // const error = useSelector(selectors.login.error);
        const isLogin = useSelector(selectors.user.isLogin);
        // const loading = useSelector(selectors.login.loading);
        const dispatch = useDispatch();

        useEffect(() => {
            loginRef.current && loginRef.current.focus();
        }, [loginRef.current]);

        async function handleSubmit(
            // event: React.FormEvent<HTMLFormElement>
            values: Values
        ) {
            // event.preventDefault();
            const { login, password } = values;

            if (login && password) {
                return dispatch(submit({ login, password }, t));
            } else {
                // dispatch(setError('Укажите логин и пароль'));
            }
        }

        // function handleLoginChange(event) {
        //     dispatch(setLogin(event.target.value));
        // }

        // function handlePasswordChange(event) {
        //     dispatch(setPassword(event.target.value));
        // }

        if (isLogin) {
            return <Redirect to={URLs.main.url} />;
        }

        function validateForm(values) {
            interface Errors {
                login?: string;
                password?: string;
            }
            const errors: Errors = {};

            if (!values.login) {
                errors.login = t('i18next.login.errorenterlogin');
            }
            if (!values.password) {
                errors.password = t('i18next.login.errorenterpassword');
            }

            return errors;
        }

        return (
            <Form
                onSubmit={handleSubmit}
                validate={validateForm}
                subscription={{ submitting: true }}
                render={({ handleSubmit }) => (
                    <form className={style.form} onSubmit={handleSubmit}>
                        <h3 className={style.formTitle}>
                            {t('i18next.login.title')}
                        </h3>
                        {/* <FormSpy
                            subscription={{ submitting: true }}
                            render={() => (
                                <Button
                                    colorScheme={ButtonColors.blue}
                                    className={style.buttonOffset}
                                >
                                    {t('i18next.login.loginwithgoogle')}
                                </Button>
                            )}
                        /> */}
                        {/* <div className={style.or}>
                            <span className={style.orLine}></span>
                            <span className={style.orText}>
                                {t('i18next.login.or')}
                            </span>
                            <span className={style.orLine}></span>
                        </div> */}
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    name="login"
                                    component={LabeledInput}
                                    inputRef={loginRef}
                                    label={`${t('i18next.login.login')}:`}
                                    id="login-input"
                                    placeholder={t('i18next.login.enterlogin')}
                                    link={{
                                        to: `${URLs.recoverLogin.url}`,
                                        text: `${t(
                                            'i18next.login.forgotlogin'
                                        )}`,
                                    }}
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    name="password"
                                    component={LabeledInput}
                                    label={t('i18next.login.password')}
                                    id="password-input"
                                    placeholder={t(
                                        'i18next.login.enterpassword'
                                    )}
                                    type="password"
                                    link={{
                                        to: `${URLs.recoverPassword.url}`,
                                        text: `${t(
                                            'i18next.login.forgotpassword'
                                        )}`,
                                    }}
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                                errors: true,
                                pristine: true,
                            }}
                            render={({ submitting, errors, pristine }) => (
                                <Button
                                    colorScheme={ButtonColors.green}
                                    className={style.buttonOffset}
                                    type="submit"
                                    disabled={
                                        submitting ||
                                        pristine ||
                                        errors.login ||
                                        errors.password
                                    }
                                    loading={submitting}
                                >
                                    {t('i18next.login.continue')}
                                </Button>
                            )}
                        />

                        <Link className={style.register} to={URLs.register.url}>
                            {t('i18next.login.register')}
                        </Link>
                    </form>
                )}
            />
            // <form className={style.form} onSubmit={handleSubmit}>
            //     <h3 className={style.formTitle}>
            //     <RenderCount />
            //         Войдите, чтобы продолжить
            //     </h3>
            //     <Button
            //         colorScheme={ButtonColors.blue}
            //         className={style.buttonOffset}
            //     >
            //         Войти с помощью Google
            //     </Button>
            //     <div className={style.or}>
            //         <span className={style.orLine}></span>
            //         <span className={style.orText}>или</span>
            //         <span className={style.orLine}></span>
            //     </div>
            //     <p style={{ marginTop: '10px' }} className={style.orText}>
            //         (существ. login-ы для проверки - login, login1; пароль
            //         пока любой)
            //     </p>
            //     <LabeledInput
            //         inputRef={loginRef}
            //         label="Логин"
            //         id="login-input"
            //         name="login"
            //         placeholder="Введите логин"
            //         value={login}
            //         onChange={handleLoginChange}
            //         link={{
            //             to: `${URLs.recoverLogin.url}`,
            //             text: 'Забыли логин?',
            //         }}
            //         error={error}
            //         disabled={loading}
            //     />
            //     <LabeledInput
            //         label="Пароль"
            //         id="password-input"
            //         name="password"
            //         placeholder="Введите пароль"
            //         type="password"
            //         value={password}
            //         onChange={handlePasswordChange}
            //         link={{
            //             to: `${URLs.recoverPassword.url}`,
            //             text: 'Забыли пароль?',
            //         }}
            //         error={!!error}
            //         disabled={loading}
            //     />
            //     <Button
            //         disabled={loading}
            //         loading={loading}
            //         colorScheme={ButtonColors.green}
            //         className={style.buttonOffset}
            //         type="submit"
            //     >
            //         Продолжить
            //     </Button>
            //     <Link className={style.register} to={URLs.register.url}>
            //         Зарегистрироваться
            //     </Link>
            // </form>
        );
    };

// function mapStateToProps({ login }): MapStateToProps {
//     return {
//         login: login.login,
//         password: login.password,
//         error: login.error,
//         isReady: login.isReady,
//         loading: login.loading,
//     };
// }

// function mapDispatchToProps(dispatch): MapDispatchToProps {
//     return {
//         submit: (data) => dispatch(submitLoginFormActionCreator(data)),
//         setLogin: (value) => dispatch(loginChangeActionCreator(value)),
//         setPassword: (value) => dispatch(passwordChangeActionCreator(value)),
//         setError: (error) => dispatch(setErrorActionCreator(error)),
//     };
// }

// Login.propTypes = {
//     login: PropTypes.string.isRequired,
//     password: PropTypes.string.isRequired,
//     error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
//     isReady: PropTypes.bool.isRequired,
//     loading: PropTypes.bool.isRequired,
//     setLogin: PropTypes.func.isRequired,
//     setPassword: PropTypes.func.isRequired,
//     setError: PropTypes.func.isRequired,
//     submit: PropTypes.func.isRequired,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Login);
export default Login;
