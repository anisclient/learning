import { lazy } from 'react';

export default lazy(() => import(/*webpackChunkName: "login-page"*/ './login'));
