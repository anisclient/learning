import { lazy } from 'react';

export default lazy(
    () => import(/*webpackChunkName: "basket-page"*/ './basket-page')
);
