import React, { useEffect, useState } from 'react';
import { URLs } from '../../__data__/urls';
import { Link } from 'react-router-dom';
import style from './style.css';
import { Button, ButtonColors } from '../../components';
import BasketIcon from '../../components/header/basket-icon';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../../__data__/store/reducers';
import { AllProducts, clearBasket } from '../../__data__/store/reducers/basket';
import BasketProduct from '../../components/basket-product';
import ModalWindow, { ModalWindowType } from '../../components/modal-window';
import { useTranslation } from 'react-i18next';
import { useLocalStorageInBasket } from '../../utils';
/* eslint-disable no-unused-vars */

const BasketPage: React.FC = () => {
    const dispatch = useDispatch();
    const { t } = useTranslation();

    const isLoginFromReducer = useSelector<AppStore, boolean>(
        ({ user }) => user.isLogin
    );

    useLocalStorageInBasket();

    const allProducts = useSelector<AppStore, AllProducts | AllProducts[]>(
        ({ basket }) => basket.allProducts
    );

    const totalCount = useSelector<AppStore, number>(
        ({ basket }) => basket.totalCount
    );
    const totalPrice = useSelector<AppStore, number>(
        ({ basket }) => basket.totalPrice
    );

    const productsByGroup = Object.keys(allProducts).map((key) => {
        return allProducts[key].productsById[0];
    });

    const [isLogin, setIsLogin] = useState(false);

    useEffect(() => {
        setIsLogin(isLoginFromReducer);
    }, [isLoginFromReducer]);

    const [modalWindowActive, changeModalWindowActive] = useState(false);

    function showPopapClear() {
        changeModalWindowActive(!modalWindowActive);
    }

    function toClearBasket() {
        showPopapClear();
        dispatch(clearBasket());
    }

    return (
        <div className={style.basket}>
            <div className={style.container}>
                <div className={style.basketBody}>
                    {productsByGroup.length === 0 ? (
                        !isLogin ? (
                            <>
                                <h3 className={style.basketTitle}>
                                    {t('i18next.cart.empty')} 😕
                                </h3>
                                <p className={style.basketText}>
                                    {t('i18next.cart.ifitemsincart')}
                                </p>
                                <Link
                                    className={style.basketLink}
                                    to={URLs.login.url}
                                >
                                    {t('i18next.cart.login')},
                                </Link>
                                <p className={style.basketText}>
                                    {t('i18next.cart.toviewthelist')}.
                                </p>
                                <Link
                                    to={URLs.main.url}
                                    className={style.basketToMainLink}
                                >
                                    <Button
                                        colorScheme={ButtonColors.green}
                                        className={style.buttonOffset}
                                    >
                                        {t('i18next.cart.tothemain')}
                                    </Button>
                                </Link>
                            </>
                        ) : (
                            <>
                                <h3 className={style.basketTitle}>
                                    {t('i18next.cart.empty')} 😕
                                </h3>
                                <Link
                                    to={URLs.main.url}
                                    className={style.basketToMainLink}
                                >
                                    <Button
                                        colorScheme={ButtonColors.green}
                                        className={style.buttonOffset}
                                    >
                                        {t('i18next.cart.tothemain')}
                                    </Button>
                                </Link>
                            </>
                        )
                    ) : (
                        <>
                            {modalWindowActive && (
                                <ModalWindow
                                    buttonText={t('i18next.cart.clear')}
                                    modalText={t(
                                        'i18next.cart.questionformodalwindow'
                                    )}
                                    switchShowPopap={showPopapClear}
                                    handler={toClearBasket}
                                    type={ModalWindowType.forBasket}
                                />
                            )}
                            <div className={style.basketProductsTitle}>
                                <div className={style.basketProductsIcon}>
                                    <BasketIcon />
                                    <h3 className={style.basketTitle}>
                                        {t('i18next.cart.title')}
                                    </h3>
                                </div>
                                <Button
                                    colorScheme={ButtonColors.transparent}
                                    onClick={showPopapClear}
                                >
                                    {t('i18next.cart.removeall')}
                                </Button>
                            </div>
                            {productsByGroup &&
                                productsByGroup.map((product, i) => (
                                    <BasketProduct
                                        product={product}
                                        allProducts={allProducts}
                                        key={i}
                                    />
                                ))}
                            <div className={style.basketProductsSum}>
                                <div className={style.basketProductsQuantity}>
                                    <span>{t('i18next.cart.totalitems')}:</span>
                                    <strong>
                                        {totalCount}{' '}
                                        {totalCount === 1
                                            ? t('i18next.cart.pc')
                                            : t('i18next.cart.pcs')}
                                        .
                                    </strong>
                                </div>
                                <div className={style.basketProductsTotalPrice}>
                                    <span>{t('i18next.cart.totalprice')}:</span>
                                    <strong>{totalPrice}</strong>
                                    <small>₽</small>
                                </div>
                            </div>
                            <div className={style.basketProductsBtns}>
                                <Link
                                    to={URLs.main.url}
                                    className={style.basketProductsToMainLink}
                                >
                                    {t('i18next.cart.tothemain')}
                                </Link>
                                <Link
                                    to={URLs.order.url}
                                    className={style.basketProductsPay}
                                >
                                    <Button
                                        colorScheme={ButtonColors.blue}
                                        className={style.basketProductsPay}
                                    >
                                        {t('i18next.cart.proceedtocheckout')}
                                    </Button>
                                </Link>
                            </div>
                        </>
                    )}
                </div>
            </div>
        </div>
    );
};

export default BasketPage;
