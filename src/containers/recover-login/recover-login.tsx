import React, { ReactElement, useState } from 'react';
import { ConfirmStage } from './steps/confirm-stage';
import { InputStage } from './steps/input-stage';

/* eslint-disable no-unused-vars */
export interface StageProps {
    setStep: (step: RecoverLoginStep) => void;
}

export enum RecoverLoginStep {
    INPUT_STEP,
    CONFIRM_STEP,
}

const steps = {
    [RecoverLoginStep.INPUT_STEP]: InputStage,
    [RecoverLoginStep.CONFIRM_STEP]: ConfirmStage,
};

function RecoverLogin(): ReactElement {
    const [currentStep, setCurrentStep] = useState(RecoverLoginStep.INPUT_STEP);
    const Stage: React.FC<StageProps> = steps[currentStep];

    return <Stage setStep={setCurrentStep} />;
}

export default RecoverLogin;
