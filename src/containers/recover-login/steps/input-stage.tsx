import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { RecoverLoginStep, StageProps } from '../recover-login';
import { Link } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import PropTypes from 'prop-types';
import {
    // emailChangeActionCreator as setEmail,
    // setErrorActionCreator as setError,
    submitRecoverLoginForm as submit,
    // resetDataActionCreator as reset,
} from '../../../__data__/actions/recover-login-input-stage';
import { handleResetData as reset } from '../../../__data__/store/slices/recover-login/input-stage';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';
// import { connect } from 'react-redux';

/* eslint-disable no-unused-vars */

// interface MapStateToProps {
//     data: Record<string, unknown>;
//     email: string;
//     error: string | boolean;
//     loading: boolean;
// }

interface Values {
    email: string;
}

// interface MapDispatchToProps {
//     submit: (data: Data) => void;
//     setEmail: (value: string) => void;
//     setError: (value: string | boolean) => void;
//     reset: () => void;
// }

// type InputStageProps = StageProps & MapStateToProps & MapDispatchToProps;

export const InputStage: React.FC<StageProps> = ({
    setStep,
    // data,
    // email,
    // error,
    // loading,
    // submit,
    // setEmail,
    // setError,
    // reset,
}) => {
    const loginRef = useRef<HTMLInputElement>();
    const { t } = useTranslation();

    const data = useSelector(selectors.recoverLogin.inputStage.data);
    // const email = useSelector(selectors.recoverLogin.inputStage.email);
    // const error = useSelector(selectors.recoverLogin.inputStage.error);
    // const loading = useSelector(selectors.recoverLogin.inputStage.loading);

    const dispatch = useDispatch();

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    useEffect(() => {
        if (data) {
            setStep(RecoverLoginStep.CONFIRM_STEP);
            dispatch(reset());
        }
    }, [data]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();
        const { email } = values;

        if (email) {
            return dispatch(submit({ email }, t));
        } else {
            // dispatch(setError('Необходимо указать email'));
        }
    }

    // function handleChange(event) {
    //     dispatch(setEmail(event.target.value));
    // }

    function validateForm(values) {
        interface Errors {
            email?: string;
        }
        const errors: Errors = {};
        if (!values.email) {
            errors.email = t('i18next.recoverlogin.input.erroremailisrequired');
        }
        return errors;
    }

    return (
        <Form
            onSubmit={handleSubmit}
            validate={validateForm}
            subscription={{ submitting: true }}
            render={({ handleSubmit }) => (
                <form className={style.form} onSubmit={handleSubmit}>
                    <h3 className={style.formTitle}>
                        {t('i18next.recoverlogin.input.title')}
                    </h3>
                    <p className={style.formText}>
                        {t('i18next.recoverlogin.input.informtext')}
                    </p>
                    <FormSpy
                        render={({ submitting }) => (
                            <Field
                                name="email"
                                component={LabeledInput}
                                inputRef={loginRef}
                                label={t(
                                    'i18next.recoverlogin.input.youremail'
                                )}
                                id="email-input"
                                placeholder={t(
                                    'i18next.recoverlogin.input.enteremail'
                                )}
                                disabled={submitting}
                            />
                        )}
                    />
                    <FormSpy
                        subscription={{
                            pristine: true,
                            errors: true,
                            submitting: true,
                        }}
                        render={({ pristine, errors, submitting }) => (
                            <Button
                                colorScheme={ButtonColors.green}
                                className={style.buttonOffset}
                                type="submit"
                                disabled={
                                    pristine || errors.email || submitting
                                }
                                loading={submitting}
                            >
                                {t('i18next.recoverlogin.input.continue')}
                            </Button>
                        )}
                    />
                    <Link to={URLs.login.url} className={style.register}>
                        {t('i18next.recoverlogin.input.cancel')}
                    </Link>
                </form>
            )}
        />
        // <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Восстановление логина</h3>
        //     <RenderCount/>
        //     <p className={style.formText}>
        //         На введенный email придет код подтверждения
        //     </p>
        //     <LabeledInput
        //         inputRef={loginRef}
        //         label="Ваш email:"
        //         id="email-input"
        //         name="email"
        //         placeholder="Введите email"
        //         value={email}
        //         onChange={handleChange}
        //         error={error}
        //         disabled={loading}
        //     />
        //     <Button
        //         disabled={loading}
        //         loading={loading}
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //     >
        //     <RenderCount/>
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.register}>
        //         Отменить
        //     </Link>
        // </form>
    );
};

// const mapStateToProps = ({
//     recoverLogin: { inputStage },
// }): MapStateToProps => ({
//     data: inputStage.data,
//     email: inputStage.email,
//     error: inputStage.error,
//     loading: inputStage.loading,
// });

// const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
//     submit: (data: Data) => dispatch(submitRecoverLoginForm(data)),
//     setEmail: (email: string) => dispatch(emailChangeActionCreator(email)),
//     setError: (error: string | boolean) =>
//         dispatch(setErrorActionCreator(error)),
//     reset: () => dispatch(resetDataActionCreator()),
// });

InputStage.propTypes = {
    // data: PropTypes.oneOfType([PropTypes.oneOf([null]), PropTypes.string]),
    // email: PropTypes.string.isRequired,
    // error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
    // loading: PropTypes.bool.isRequired,
    // submit: PropTypes.func.isRequired,
    // setEmail: PropTypes.func.isRequired,
    // setError: PropTypes.func.isRequired,
    // reset: PropTypes.func.isRequired,
    setStep: PropTypes.func.isRequired,
};

// export default connect(mapStateToProps, mapDispatchToProps)(InputStage);
