import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { Link } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
// import { connect } from 'react-redux';
import {
    recLoginConfirmFormSubmit as submit,
    // setCodeActionCreator as setCode,
    // setErrorActionCreator as setError,
    // setResetData as reset,
} from '../../../__data__/actions/recover-login-confirm-stage';
import { handleResetData as reset } from '../../../__data__/store/slices/recover-login/confirm-stage';
// import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

// interface MapStateToProps {
//     code: string;
//     error: string | boolean;
//     loading: boolean;
//     recoveredLogin: string;
// }

interface Values {
    code: number;
}

// interface MapDispatchToProps {
//     submit: (data: Data) => void;
//     setCode: (code: string) => void;
//     setError: (error: string | boolean) => void;
//     reset: () => void;
// }

// type ConfirmStageProps = MapStateToProps & MapDispatchToProps;

export const ConfirmStage: React.FC = () =>
    // {
    // code,
    // error,
    // loading,
    // recoveredLogin,
    // submit,
    // setCode,
    // setError,
    // reset,
    // }
    {
        const loginRef = useRef<HTMLInputElement>();
        const { t } = useTranslation();

        // const code = useSelector(selectors.recoverLogin.confirmStage.code);
        // const error = useSelector(selectors.recoverLogin.confirmStage.error);
        // const loading = useSelector(
        //     selectors.recoverLogin.confirmStage.loading
        // );
        const recoveredLogin = useSelector(
            selectors.recoverLogin.confirmStage.recoveredLogin
        );

        const dispatch = useDispatch();

        useEffect(() => {
            loginRef.current && loginRef.current.focus();
        }, [loginRef.current]);

        useEffect(() => {
            dispatch(reset());
        }, []);

        async function handleSubmit(
            // event: React.FormEvent<HTMLFormElement>
            values: Values
        ) {
            // event.preventDefault();
            const { code } = values;

            if (code) {
                return dispatch(submit({ code }, t));
            }
            // else {
            // dispatch(setError('Укажите проверочный код'));
            // }
        }

        // function handleChange(event) {
        //     dispatch(setCode(event.target.value));
        // }

        function validateForm(values) {
            interface Errors {
                code?: string;
            }
            const errors: Errors = {};

            if (!values.code) {
                errors.code = t(
                    'i18next.recoverlogin.confirm.errorecodeisrequired'
                );
            }

            return errors;
        }

        return (
            <Form
                onSubmit={handleSubmit}
                validate={validateForm}
                subscription={{ submitting: true }}
                render={({ handleSubmit }) => (
                    <form className={style.form} onSubmit={handleSubmit}>
                        <h3 className={style.formTitle}>
                            {t('i18next.recoverlogin.confirm.confirmation')}
                        </h3>
                        {!recoveredLogin && (
                            <>
                                <p className={style.formText}>
                                    {t(
                                        'i18next.recoverlogin.confirm.informtext'
                                    )}
                                </p>
                                <FormSpy
                                    render={({ submitting }) => (
                                        <Field
                                            name="code"
                                            component={LabeledInput}
                                            inputRef={loginRef}
                                            label={t(
                                                'i18next.recoverlogin.confirm.code'
                                            )}
                                            id="code-input"
                                            placeholder={t(
                                                'i18next.recoverlogin.confirm.entercode'
                                            )}
                                            type="number"
                                            disabled={submitting}
                                            // disabled={loading}
                                            // error={error}
                                        />
                                    )}
                                />
                            </>
                        )}
                        {recoveredLogin && (
                            <div className={style.recovered}>
                                <h3 className={style.formTitle}>
                                    {t(
                                        'i18next.recoverlogin.confirm.yourlogin'
                                    )}
                                </h3>
                                <p className={style.formText}>
                                    {recoveredLogin}
                                </p>
                            </div>
                        )}
                        {!recoveredLogin ? (
                            <>
                                <FormSpy
                                    subscription={{
                                        pristine: true,
                                        submitting: true,
                                        errors: true,
                                    }}
                                    render={({
                                        pristine,
                                        submitting,
                                        errors,
                                    }) => (
                                        <Button
                                            colorScheme={ButtonColors.green}
                                            className={style.buttonOffset}
                                            type="submit"
                                            disabled={
                                                pristine ||
                                                errors.code ||
                                                submitting
                                            }
                                            loading={submitting}
                                            // loading={loading}
                                            // disabled={loading}
                                        >
                                            {t(
                                                'i18next.recoverlogin.confirm.continue'
                                            )}
                                        </Button>
                                    )}
                                />
                                <Link
                                    to={URLs.login.url}
                                    className={style.register}
                                >
                                    {t('i18next.recoverlogin.confirm.cancel')}
                                </Link>
                            </>
                        ) : (
                            <Link to={URLs.login.url} className={style.auth}>
                                {t('i18next.recoverlogin.confirm.login')}
                            </Link>
                        )}
                    </form>
                )}
            />
            // <form className={style.form} onSubmit={handleSubmit}>
            //     <h3 className={style.formTitle}>Подтверждение</h3>
            //     <p className={style.formText}>
            //         Введите код (правильный код - 123)
            //     </p>
            //     {!recoveredLogin && (
            //         <LabeledInput
            //             inputRef={loginRef}
            //             label="Код:"
            //             id="code-input"
            //             name="code"
            //             placeholder="Введите код"
            //             value={code}
            //             onChange={handleChange}
            //             type="number"
            //             error={error}
            //             disabled={loading}
            //         />
            //     )}
            //     {recoveredLogin && (
            //         <div className={style.recovered}>
            //             <h3 className={style.formTitle}>Ваш логин:</h3>
            //             <p className={style.formText}>{recoveredLogin}</p>
            //         </div>
            //     )}
            //     {!recoveredLogin ? (
            //         <>
            //             <Button
            //                 colorScheme={ButtonColors.green}
            //                 className={style.buttonOffset}
            //                 type="submit"
            //                 disabled={loading}
            //                 loading={loading}
            //             >
            //                 Продолжить
            //             </Button>
            //             <Link to={URLs.login.url} className={style.register}>
            //                 Отменить
            //             </Link>
            //         </>
            //     ) : (
            //         <Link to={URLs.login.url} className={style.auth}>
            //             Авторизоваться
            //         </Link>
            //     )}
            // </form>
        );
    };

// const mapStateToProps = ({
//     recoverLogin: { confirmStage },
// }): MapStateToProps => ({
//     code: confirmStage.code,
//     error: confirmStage.error,
//     loading: confirmStage.loading,
//     recoveredLogin: confirmStage.recoveredLogin,
// });

// const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
//     submit: (data: Data) => dispatch(recLoginConfirmFormSubmit(data)),
//     setCode: (code: string) => dispatch(setCodeActionCreator(code)),
//     setError: (error: string | boolean) =>
//         dispatch(setErrorActionCreator(error)),
//     reset: () => dispatch(setResetData()),
// });

// ConfirmStage.propTypes = {
//     code: PropTypes.string.isRequired,
//     error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
//     loading: PropTypes.bool.isRequired,
//     recoveredLogin: PropTypes.string.isRequired,
//     submit: PropTypes.func.isRequired,
//     setCode: PropTypes.func.isRequired,
//     setError: PropTypes.func.isRequired,
//     reset: PropTypes.func.isRequired,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(ConfirmStage);
