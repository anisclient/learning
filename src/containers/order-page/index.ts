import { lazy } from 'react';

export default lazy(
    () => import(/*webpackChunkName: "order-page"*/ './order-page')
);
