import { LabeledInput, Button, ButtonColors } from '../../components';
import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { Link, useHistory } from 'react-router-dom';
import { URLs } from '../../__data__/urls';
import { useDispatch } from 'react-redux';
import { Field, Form, FormSpy } from 'react-final-form';
import RadioInput from '../../components/radio-input';
import TextArea from '../../components/textarea';
import { AllProducts, clearBasket } from '../../__data__/store/reducers/basket';
import { useSelector } from 'react-redux';
import { AppStore } from '../../__data__/store/reducers';
// import { getConfigValue } from '@ijl/cli';
import ModalWindow, { ModalWindowType } from '../../components/modal-window';
import { useTranslation } from 'react-i18next';
import { baseAxios } from '../../utils';

/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-escape */

type DeliveryMethod = 'pickup' | 'shipping';

interface Values {
    name: string;
    phone: string;
    delivery?: DeliveryMethod;
    address?: string;
}

const OrderPage: React.FC = () => {
    const { t } = useTranslation();

    const orderRef = useRef<HTMLInputElement>();
    const history = useHistory();

    const [modalWindowActive, changeModalWindowActive] = useState(false);
    const [text, setText] = useState('');

    const allProducts = useSelector<AppStore, AllProducts | AllProducts[]>(
        ({ basket }) => basket.allProducts
    );

    const productsByGroup = Object.keys(allProducts).map((key) => {
        return allProducts[key].productsById[0];
    });

    if (productsByGroup.length === 0) {
        history.push(`${URLs.main.url}`);
    }

    const dispatch = useDispatch();

    useEffect(() => {
        orderRef.current && orderRef.current.focus();
    }, [orderRef.current]);

    function handleShowPopap() {
        changeModalWindowActive(!modalWindowActive);
    }

    function toClearBasket() {
        handleShowPopap();
        window.localStorage.removeItem('products-in-basket-local-storage');
        dispatch(clearBasket());
    }

    async function handleSubmit(values: Values) {
        const { name, phone, address } = values;

        if (name && phone) {
            // const baseApiUrl = getConfigValue('e_zone.api');
            ////////////////////////start
            // const response = await fetch(baseApiUrl + '/order', {
            //     method: 'POST',
            //     headers: {
            //         'Content-Type': 'application/json;charset=utf-8',
            //     },
            //     body: JSON.stringify({
            //         name,
            //         phone,
            //         address: address ? address : 'pickup',
            //     }),
            // });

            // if (response.ok) {
            //     try {
            //         const result = await response.json();
            //         setText(
            //             `${t('i18next.orderpage.messageformodalwindow')}${
            //                 result.message
            //             }`
            //         );
            //         handleShowPopap();
            //     } catch (error) {
            //         console.error(error.message);
            //         return { name: error.message };
            //     }
            // } else {
            //     try {
            //         const result = await response.json();
            //         return { name: result.error };
            //     } catch (error) {
            //         console.error(error);
            //     }
            // }
            ////////////////////////end
            try {
                const response = await baseAxios({
                    method: 'POST',
                    url: '/order',
                    data: {
                        name,
                        phone,
                        address: address ? address : 'pickup',
                    },
                });
                setText(
                    `${t('i18next.orderpage.messageformodalwindow')}${
                        response.data.message
                    }`
                );
                handleShowPopap();
            } catch (error) {
                return { name: error?.response?.data?.error };
            }
        }
    }

    function validateForm(values) {
        interface Errors {
            name?: string;
            phone?: string;
            address?: string;
        }
        const errors: Errors = {};

        if (!values.name) {
            errors.name = t('i18next.orderpage.errornameisrequired');
        }

        if (!values.phone) {
            errors.phone = t('i18next.orderpage.errorphoneisrequired');
        }

        if (
            values.phone &&
            !/^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/.test(
                values.phone
            )
        ) {
            errors.phone = t('i18next.orderpage.errorvalidnumber');
        }

        if (values.delivery === 'shipping' && !values.address) {
            errors.address = t('i18next.orderpage.erroraddressisrequired');
        }

        return errors;
    }

    return (
        <>
            {modalWindowActive && (
                <ModalWindow
                    buttonText="Ok"
                    modalText={text}
                    switchShowPopap={handleShowPopap}
                    handler={toClearBasket}
                    type={ModalWindowType.onlyInformationType}
                />
            )}
            <Form
                onSubmit={handleSubmit}
                initialValues={{ delivery: 'pickup' }}
                validate={validateForm}
                subscription={{ submitting: true }}
                render={({ handleSubmit }) => (
                    <form className={style.form} onSubmit={handleSubmit}>
                        <h3 className={style.formTitle}>
                            {t('i18next.orderpage.ordering')}
                        </h3>
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    name="name"
                                    component={LabeledInput}
                                    inputRef={orderRef}
                                    label={`${t('i18next.orderpage.name')}:`}
                                    id="name"
                                    placeholder={t(
                                        'i18next.orderpage.enteryourname'
                                    )}
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    name="phone"
                                    component={LabeledInput}
                                    label={`${t('i18next.orderpage.phone')}:`}
                                    id="phone"
                                    placeholder={t(
                                        'i18next.orderpage.enterphonenumber'
                                    )}
                                    type="tel"
                                    disabled={submitting}
                                />
                            )}
                        />

                        <div className={style.or}>
                            <span className={style.orLine}></span>
                            <span className={style.orText}>
                                {`${t('i18next.orderpage.obtainingway')}:`}
                            </span>
                            <span className={style.orLine}></span>
                        </div>

                        <div className={style.delivery}>
                            <Field<DeliveryMethod>
                                name="delivery"
                                component={RadioInput}
                                type="radio"
                                value="pickup"
                                id="pickup"
                            >
                                {t('i18next.orderpage.pickup')}
                            </Field>
                            <Field<DeliveryMethod>
                                name="delivery"
                                component={RadioInput}
                                type="radio"
                                value="shipping"
                                id="shipping"
                            >
                                {t('i18next.orderpage.shipping')}
                            </Field>
                        </div>
                        <FormSpy
                            subscription={{
                                values: true,
                                submitting: true,
                            }}
                            render={({ values, submitting }) => {
                                return (
                                    values.delivery === 'shipping' && (
                                        <Field
                                            name="address"
                                            component={TextArea}
                                            placeholder={t(
                                                'i18next.orderpage.enterdeliveryaddress'
                                            )}
                                            disabled={submitting}
                                        />
                                    )
                                );
                            }}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                                errors: true,
                                pristine: true,
                                values: true,
                            }}
                            render={({
                                submitting,
                                errors,
                                pristine,
                                values,
                            }) => (
                                <Button
                                    colorScheme={ButtonColors.green}
                                    className={style.buttonOffset}
                                    type="submit"
                                    disabled={
                                        submitting ||
                                        pristine ||
                                        errors.name ||
                                        errors.phone ||
                                        (values.delivery === 'shipping' &&
                                            errors.address)
                                    }
                                    loading={submitting}
                                >
                                    {t('i18next.orderpage.continue')}
                                </Button>
                            )}
                        />
                        <Link className={style.back} to={URLs.basket.url}>
                            {t('i18next.orderpage.back')}
                        </Link>
                    </form>
                )}
            />
        </>
    );
};

export default OrderPage;
