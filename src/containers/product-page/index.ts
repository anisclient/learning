import { lazy } from 'react';

export default lazy(
    () => import(/*webpackChunkName: "product-page"*/ './product-page')
);
