import LikeIcon from '../../components/header/like-icon';
import React from 'react';
import style from './style.css';
import Star from '../../components/product/star';
import { Button, ButtonColors } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import {
    // chooseProductToSingleProductPage,
    likeProduct,
    unlikeProduct,
} from '../../__data__/actions/products';
import { AppStore } from '../../__data__/store/reducers';
import {
    addToBasket as addToBasketReducer,
    AllProducts,
} from '../../__data__/store/reducers/basket';
import * as selectors from '../../__data__/selectors';
import { useTranslation } from 'react-i18next';
import {
    useLocalStorageInBasket,
    useLocalStorageToLikeProduct,
} from '../../utils';
import { useParams } from 'react-router-dom';
/* eslint-disable no-unused-vars */

interface ProductId {
    productId: string;
}

const ProductPage: React.FC = () => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { productId } = useParams<ProductId>();
    // const isLoaded = useSelector(selectors.allProducts.isLoaded);

    useLocalStorageInBasket();
    useLocalStorageToLikeProduct();

    // const product = useSelector(selectors.allProducts.singleProduct);
    const allProducts = useSelector(selectors.allProducts.productsInMain);

    const product = allProducts?.find((product) => product.id === +productId);

    const productsByIdFromBasket = useSelector<AppStore, AllProducts>(
        ({ basket }) => basket.allProducts[product?.id]
    );

    function handleLike(e) {
        dispatch(likeProduct(product));
        e.preventDefault();
    }

    function handleUnlike(e) {
        dispatch(unlikeProduct(product));
        e.preventDefault();
    }

    function addToBasket() {
        dispatch(addToBasketReducer(product));
    }

    return (
        <div className={style.productPage}>
            <div className={style.container}>
                {/* {!isLoaded ? (
                    <p>Loading...</p>
                ) : ( */}
                <div className={style.productPageBody}>
                    <div className={style.productPageImg}>
                        <img
                            src={product && product?.image}
                            alt={
                                product &&
                                localStorage.getItem('i18nextLng') === 'ru'
                                    ? product?.title.ru
                                    : product?.title.en
                            }
                        />
                    </div>
                    <div className={style.productPageRight}>
                        <div className={style.productPageDescription}>
                            <div className={style.productPageTitle}>
                                {product &&
                                localStorage.getItem('i18nextLng') === 'ru'
                                    ? product?.title.ru
                                    : product?.title.en}
                            </div>
                            <div className={style.productPageLine}></div>

                            {product && product?.liked ? (
                                <div className={style.productPageLikeWrapper}>
                                    <span>
                                        {t('i18next.product.addedtofavorites')}
                                    </span>
                                    <button
                                        className={style.active}
                                        onClick={(e) => handleUnlike(e)}
                                        title={t(
                                            'i18next.product.removefromfavorites'
                                        )}
                                    >
                                        <LikeIcon />
                                    </button>
                                </div>
                            ) : (
                                <div className={style.productPageLikeWrapper}>
                                    <span>
                                        {t('i18next.product.addtofavorites')}
                                    </span>
                                    <button
                                        onClick={(e) => handleLike(e)}
                                        title={t(
                                            'i18next.product.addtofavorites'
                                        )}
                                    >
                                        <LikeIcon />
                                    </button>
                                </div>
                            )}

                            <div className={style.productPagePrice}>
                                <span>{t('i18next.product.price')}</span>
                                <p>
                                    <strong>{product && product?.price}</strong>
                                    <small>₽</small>
                                </p>
                            </div>
                            <div className={style.productPageRating}>
                                {Array(product && product?.rating)
                                    .fill(product && product?.rating)
                                    .map((_, i) => (
                                        <span
                                            role="img"
                                            aria-label="star"
                                            key={i}
                                        >
                                            <Star />
                                        </span>
                                    ))}
                            </div>
                        </div>
                        <Button
                            colorScheme={ButtonColors.green}
                            className={style.productPageButton}
                            onClick={addToBasket}
                        >
                            {productsByIdFromBasket
                                ? `${t('i18next.button.added')}: ${
                                      productsByIdFromBasket &&
                                      productsByIdFromBasket.productsById.length
                                  } ${
                                      productsByIdFromBasket &&
                                      productsByIdFromBasket.productsById
                                          .length === 1
                                          ? t('i18next.button.pc')
                                          : t('i18next.button.pcs')
                                  }.`
                                : t('i18next.button.addtocart')}
                        </Button>
                    </div>
                </div>
                {/* )} */}
            </div>
        </div>
    );
};

export default ProductPage;
