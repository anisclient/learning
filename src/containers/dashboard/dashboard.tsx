import { URLs } from '../../__data__/urls';
import React, { useState } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Login from '../login';
import { Header, LazyComponent } from '../../components';
import Main from '../main';
import BasketPage from '../basket-page';
import ProductPage from '../product-page';
import LikePage from '../like-page';
import RecoverLogin from '../recover-login';
import RecoverPassword from '../recover-password';
import Registration from '../registration';
import OrderPage from '../order-page';
import MetaTags from 'react-meta-tags';
import { favicon } from '../../assets';

const Dashboard: React.FC = () => {
    const [burgerActive, setBurgerActive] = useState(false);

    return (
        <>
            <MetaTags>
                <title>e_zone</title>
                <link rel="icon" href={favicon} type="image/png" />
            </MetaTags>
            <Header active={burgerActive} setActive={setBurgerActive} />
            <Switch>
                <Route exact path={URLs.root.url}>
                    <Redirect to={URLs.main.url} />
                </Route>
                <Route path={URLs.main.url}>
                    <LazyComponent>
                        <Main />
                    </LazyComponent>
                </Route>
                <Route path={URLs.login.url}>
                    <LazyComponent>
                        <Login />
                    </LazyComponent>
                </Route>
                <Route path={URLs.basket.url}>
                    <LazyComponent>
                        <BasketPage />
                    </LazyComponent>
                </Route>
                <Route path={`${URLs.product.url}/:productId`}>
                    <LazyComponent>
                        <ProductPage />
                    </LazyComponent>
                </Route>
                <Route path={URLs.order.url}>
                    <LazyComponent>
                        <OrderPage />
                    </LazyComponent>
                </Route>
                <Route path={URLs.like.url}>
                    <LazyComponent>
                        <LikePage />
                    </LazyComponent>
                </Route>
                <Route path={URLs.register.url}>
                    <LazyComponent>
                        <Registration />
                    </LazyComponent>
                </Route>
                <Route path={URLs.recoverLogin.url}>
                    <LazyComponent>
                        <RecoverLogin />
                    </LazyComponent>
                </Route>
                <Route path={URLs.recoverPassword.url}>
                    <LazyComponent>
                        <RecoverPassword />
                    </LazyComponent>
                </Route>
                <Route path="*">
                    <h1 style={{ padding: '100px', color: '#a84f5b' }}>
                        Page not found
                    </h1>
                </Route>
            </Switch>
        </>
    );
};

export default Dashboard;
