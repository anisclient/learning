import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { RegistrationStep, StageProps } from '../registration';
import { Link } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import {
    // formLoginChange,
    // formEmailChange,
    // formPasswordChange,
    // formRepeatPasswordChange,
    submitRegistrationForm as submit,
    // setCommonError,
    // setSamePasswordError,
    // setReset,
} from '../../../__data__/actions/registration-input-stage';
import { handleResetData as setReset } from '../../../__data__/store/slices/registration/inpus-stage';
import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

interface Values {
    login: string;
    password: string;
    repeatPassword: string;
    email: string;
}

export const InputStage: React.FC<StageProps> = ({ setStep }) => {
    const { t } = useTranslation();
    const loginRef = useRef<HTMLInputElement>();
    // const loading = useSelector(selectors.registration.inputStage.loading);
    const data = useSelector(selectors.registration.inputStage.data);
    // const error = useSelector(selectors.registration.inputStage.error);
    // const login = useSelector(selectors.registration.inputStage.login);
    // const email = useSelector(selectors.registration.inputStage.email);
    // const password = useSelector(selectors.registration.inputStage.password);
    // const repeatPassword = useSelector(
    //     selectors.registration.inputStage.repeatPassword
    // );
    // const commonError = useSelector(
    //     selectors.registration.inputStage.commonError
    // );
    // const samePasswordsError = useSelector(
    //     selectors.registration.inputStage.samePasswordsError
    // );

    const dispatch = useDispatch();

    useEffect(() => {
        if (data) {
            setStep(RegistrationStep.CONFIRM_STEP);
            dispatch(setReset());
        }
    }, [data]);

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();

        const { login, password, repeatPassword, email } = values;

        if (login && password && repeatPassword && email) {
            if (password === repeatPassword) {
                return dispatch(submit({ login, password, email }, t));
            }
            // else {
            // dispatch(setSamePasswordError('Пароли не совпадают'));
            // }
        }
        // else {
        // dispatch(setCommonError('Заполните все поля'));
        // }
    }

    // function handleLoginChange(event) {
    //     dispatch(formLoginChange(event.target.value));
    // }

    // function handleEmailChange(event) {
    //     dispatch(formEmailChange(event.target.value));
    // }

    // function handlePasswordChange(event) {
    //     dispatch(formPasswordChange(event.target.value));
    // }

    // function handleRepeatPasswordChange(event) {
    //     dispatch(formRepeatPasswordChange(event.target.value));
    // }

    function validateForm(values) {
        interface Errors {
            login?: string;
            email?: string;
            password?: string;
            repeatPassword?: string;
        }
        const errors: Errors = {};
        if (!values.login) {
            errors.login = t('i18next.registration.input.errorfillall');
        }
        if (!values.password) {
            errors.password = t('i18next.registration.input.errorfillall');
        }
        if (!values.email) {
            errors.email = t('i18next.registration.input.errorfillall');
        }
        if (!values.repeatPassword) {
            errors.repeatPassword = t(
                'i18next.registration.input.errorfillall'
            );
        } else if (values.password !== values.repeatPassword) {
            errors.repeatPassword = t(
                'i18next.registration.input.errormismatchedpasswords'
            );
        }
        if (
            values.email &&
            !/^[\w.]{2,64}@[a-z]{2,16}\.[a-z]{2,3}$/i.test(values.email)
        ) {
            errors.email = t('i18next.registration.input.errorvalidemail');
        }
        if (values.password && !/[!?\-&_*]/.test(values.password)) {
            errors.password = `${t(
                'i18next.registration.input.erroratleastoneof'
            )} !,?,-,&,_,*`;
        }
        if (values.password && /(.)\1{2}/.test(values.password)) {
            errors.password = t(
                'i18next.registration.input.erroridenticalsymbols'
            );
        }

        return errors;
    }

    return (
        <Form
            onSubmit={handleSubmit}
            validate={validateForm}
            subscription={{ submitting: true }}
            render={({ handleSubmit }) => (
                <form className={style.form} onSubmit={handleSubmit}>
                    <h3 className={style.formTitle}>
                        {t('i18next.registration.input.title')}
                    </h3>
                    <p className={style.formText}>
                        {t('i18next.registration.input.informtext')}
                    </p>
                    <FormSpy
                        subscription={{ submitting: true }}
                        render={({ submitting }) => (
                            <Field
                                name="login"
                                component={LabeledInput}
                                inputRef={loginRef}
                                label={t('i18next.registration.input.login')}
                                id="login"
                                placeholder={t(
                                    'i18next.registration.input.enterlogin'
                                )}
                                disabled={submitting}
                            />
                        )}
                    />

                    <FormSpy
                        subscription={{ submitting: true }}
                        render={({ submitting }) => (
                            <Field
                                name="password"
                                component={LabeledInput}
                                label={t('i18next.registration.input.password')}
                                id="password"
                                placeholder={t(
                                    'i18next.registration.input.enterpassword'
                                )}
                                type="password"
                                disabled={submitting}
                            />
                        )}
                    />

                    <FormSpy
                        subscription={{ submitting: true }}
                        render={({ submitting }) => (
                            <Field
                                name="repeatPassword"
                                component={LabeledInput}
                                label={`${t(
                                    'i18next.registration.input.repeatpassword'
                                )}:`}
                                id="repeat-password"
                                placeholder={t(
                                    'i18next.registration.input.repeatpassword'
                                )}
                                type="password"
                                disabled={submitting}
                            />
                        )}
                    />

                    <FormSpy
                        subscription={{ submitting: true }}
                        render={({ submitting }) => (
                            <Field
                                name="email"
                                component={LabeledInput}
                                label="Email:"
                                id="email"
                                placeholder={t(
                                    'i18next.registration.input.enteremail'
                                )}
                                disabled={submitting}
                            />
                        )}
                    />

                    <FormSpy
                        subscription={{
                            submitting: true,
                            errors: true,
                            pristine: true,
                        }}
                        render={({ submitting, errors, pristine }) => (
                            <Button
                                colorScheme={ButtonColors.green}
                                className={style.buttonOffset}
                                type="submit"
                                disabled={
                                    pristine ||
                                    errors.login ||
                                    errors.password ||
                                    errors.repeatPassword ||
                                    errors.email ||
                                    submitting
                                }
                                loading={submitting}
                            >
                                {t('i18next.registration.input.continue')}
                            </Button>
                        )}
                    />
                    <Link to={URLs.login.url} className={style.cancelLink}>
                        {t('i18next.registration.input.cancel')}
                    </Link>
                </form>
            )}
        />
        // <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Регистрация</h3>
        //     <p className={style.formText}>
        //         Заполните поля (существ. email для проверки - a@mail.com,
        //         b@mail.com)
        //     </p>
        //     <LabeledInput
        //         disabled={loading}
        //         inputRef={loginRef}
        //         label="Логин:"
        //         id="login"
        //         name="login"
        //         placeholder="Введите логин"
        //         value={login}
        //         onChange={handleLoginChange}
        //         error={commonError}
        //     />
        //     <LabeledInput
        //         disabled={loading}
        //         label="Пароль:"
        //         id="password"
        //         name="password"
        //         placeholder="Введите пароль"
        //         value={password}
        //         onChange={handlePasswordChange}
        //         error={!!commonError || samePasswordsError}
        //         type="password"
        //     />
        //     <LabeledInput
        //         disabled={loading}
        //         label="Повторите пароль:"
        //         id="repeat-password"
        //         name="repeat-password"
        //         placeholder="Повторите пароль"
        //         value={repeatPassword}
        //         onChange={handleRepeatPasswordChange}
        //         error={!!commonError || !!samePasswordsError}
        //         type="password"
        //     />
        //     <LabeledInput
        //         disabled={loading}
        //         label="Email:"
        //         id="email"
        //         name="email"
        //         placeholder="Введите email"
        //         value={email}
        //         onChange={handleEmailChange}
        //         error={!!commonError || error}
        //     />
        //     <Button
        //         disabled={loading}
        //         loading={loading}
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //     >
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.cancelLink}>
        //         Отменить
        //     </Link>
        // </form>
    );
};

InputStage.propTypes = {
    setStep: PropTypes.func.isRequired,
};
