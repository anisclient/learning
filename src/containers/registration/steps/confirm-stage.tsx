import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { Link, Redirect } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import { useSelector, useDispatch } from 'react-redux';
import {
    // formCodeChange,
    submitCodeForm as submit,
    // errorActionCreator,
    // successActionCreator,
    // setIsReadyActionCreator,
} from '../../../__data__/actions/registration-confirm-stage';
import {
    handleSuccess as successActionCreator,
    handleIsReady as setIsReadyActionCreator,
} from '../../../__data__/store/slices/registration/confirm-stage';
import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import ModalWindow, { ModalWindowType } from '../../../components/modal-window';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

interface Values {
    code: number;
}

export const ConfirmStage: React.FC = () => {
    const loginRef = useRef<HTMLInputElement>();
    const { t } = useTranslation();

    const [modalWindowActive, changeModalWindowActive] = useState(false);
    const [text, setText] = useState('');

    // const code = useSelector(selectors.registration.confirmStage.code);
    // const error = useSelector(selectors.registration.confirmStage.error);
    const isReady = useSelector(selectors.registration.confirmStage.isReady);
    // const loading = useSelector(selectors.registration.confirmStage.loading);

    const dispatch = useDispatch();

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();
        const { code } = values;

        if (code) {
            return submit({ code }, setText, handleShowPopap, t);
        } else {
            // dispatch(errorActionCreator('Укажите проверочный код'));
        }
    }
    // function handleChange(event) {
    //     dispatch(formCodeChange(event.target.value));
    // }

    useEffect(() => {
        return function () {
            dispatch(setIsReadyActionCreator(false));
        };
    }, []);

    if (isReady) {
        return <Redirect to={URLs.login.url} />;
    }

    function handleShowPopap() {
        changeModalWindowActive(!modalWindowActive);
    }

    function finishRegistration() {
        handleShowPopap();
        dispatch(successActionCreator());
    }

    function validateForm(values) {
        interface Errors {
            code?: string;
        }
        const errors: Errors = {};
        if (!values.code) {
            errors.code = t(
                'i18next.registration.confirm.errorecodeisrequired'
            );
        }
        return errors;
    }

    return (
        <>
            {modalWindowActive && (
                <ModalWindow
                    buttonText="Ok"
                    modalText={text}
                    switchShowPopap={handleShowPopap}
                    handler={finishRegistration}
                    type={ModalWindowType.onlyInformationType}
                />
            )}
            <Form
                onSubmit={handleSubmit}
                validate={validateForm}
                subscription={{ submitting: true }}
                render={({ handleSubmit }) => (
                    <form className={style.form} onSubmit={handleSubmit}>
                        <h3 className={style.formTitle}>
                            {t('i18next.registration.confirm.title')}
                        </h3>
                        <p className={style.formText}>
                            {t('i18next.registration.confirm.informtext')}
                        </p>
                        <FormSpy
                            subscription={{ submitting: true }}
                            render={({ submitting }) => (
                                <Field
                                    name="code"
                                    component={LabeledInput}
                                    inputRef={loginRef}
                                    label={t(
                                        'i18next.registration.confirm.code'
                                    )}
                                    id="code-input"
                                    placeholder={t(
                                        'i18next.registration.confirm.entercode'
                                    )}
                                    type="number"
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                                errors: true,
                                pristine: true,
                            }}
                            render={({ submitting, errors, pristine }) => (
                                <Button
                                    colorScheme={ButtonColors.green}
                                    className={style.buttonOffset}
                                    type="submit"
                                    disabled={
                                        pristine || errors.code || submitting
                                    }
                                    loading={submitting}
                                >
                                    {t('i18next.registration.confirm.continue')}
                                </Button>
                            )}
                        />
                        <Link to={URLs.login.url} className={style.cancelLink}>
                            {t('i18next.registration.confirm.cancel')}
                        </Link>
                    </form>
                )}
            />
        </>
        // <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Завершение регистрации</h3>
        //     <p className={style.formText}>Введите код (правильный код - 123)</p>
        //     <LabeledInput
        //         inputRef={loginRef}
        //         label="Код:"
        //         id="code-input"
        //         name="code"
        //         placeholder="Введите код"
        //         value={code}
        //         onChange={handleChange}
        //         type="number"
        //         error={error}
        //         disabled={loading}
        //     />
        //     <Button
        //         disabled={loading}
        //         loading={loading}
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //     >
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.cancelLink}>
        //         Отменить
        //     </Link>
        // </form>
    );
};
