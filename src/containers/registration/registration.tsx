import React, { ReactElement, useState } from 'react';
import { ConfirmStage } from './steps/confirm-stage';
import { InputStage } from './steps/input-stage';

/* eslint-disable no-unused-vars */
export interface StageProps {
    setStep: (step: RegistrationStep) => void;
}

export enum RegistrationStep {
    INPUT_STEP,
    CONFIRM_STEP,
}

const steps = {
    [RegistrationStep.INPUT_STEP]: InputStage,
    [RegistrationStep.CONFIRM_STEP]: ConfirmStage,
};

function Registration(): ReactElement {
    const [currentStep, setCurrentStep] = useState(RegistrationStep.INPUT_STEP);
    const Stage: React.FC<StageProps> = steps[currentStep];

    return <Stage setStep={setCurrentStep} />;
}

export default Registration;
