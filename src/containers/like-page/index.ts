import { lazy } from 'react';

export default lazy(
    () => import(/*webpackChunkName: "like-page"*/ './like-page')
);
