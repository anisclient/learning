import Product from '../../components/product';
import React from 'react';
import style from './style.css';
import { ProductViewType } from '../../components/product/product';
// import PropTypes from 'prop-types';
// import { ProductsItem } from '../../__data__/store/reducers/products';
import { useTranslation } from 'react-i18next';
// import { likeProduct } from '../../__data__/actions/products';
// import * as selectors from '../../__data__/selectors';
// import { useDispatch, useSelector } from 'react-redux';
import {
    useLocalStorageInBasket,
    useLocalStorageToLikeProduct,
} from '../../utils';

// interface LikePageProps {
// dataToLikePage?: ProductsItem[];
// }

const LikePage: React.FC = () => {
    const { t } = useTranslation();

    useLocalStorageInBasket();
    const likedProducts = useLocalStorageToLikeProduct();

    // const dispatch = useDispatch();

    // const likedProducts = useSelector(selectors.allProducts.likedProducts);

    // const [likedProductsState, setLikedProductsState] = useState(
    //     likedProducts.length === 0
    //         ? window.localStorage['liked-products-in-local-storage'] &&
    //               JSON.parse(
    //                   window.localStorage['liked-products-in-local-storage']
    //               )
    //         : likedProducts
    // );

    // useEffect(() => {
    //     if (
    //         likedProducts.length === 0 &&
    //         window.localStorage['liked-products-in-local-storage']
    //     ) {
    //         const likedProductsFromLocalStorageArr = JSON.parse(
    //             window.localStorage['liked-products-in-local-storage']
    //         );

    //         likedProductsFromLocalStorageArr.forEach((product) => {
    //             dispatch(likeProduct(product));
    //         });
    //     }
    // }, []);

    // useEffect(() => {
    //     if (likedProducts) {
    //         setLikedProductsState(likedProducts);
    //         window.localStorage.setItem(
    //             'liked-products-in-local-storage',
    //             JSON.stringify(likedProducts)
    //         );
    //     }
    // }, [likedProducts]);

    return (
        <div className={style.likePage}>
            <div className={style.container}>
                <h3 className={style.likePageTitle}>
                    {t('i18next.likepage.title')}
                </h3>
                <div className={style.likePageBody}>
                    {likedProducts &&
                        likedProducts.map((product) => (
                            <Product
                                key={product.id}
                                type={ProductViewType.forLike}
                                product={product}
                            />
                        ))}
                </div>
                {!likedProducts?.length && (
                    <p className={style.likePageText}>
                        {t('i18next.likepage.textifnothing')} 😕
                    </p>
                )}
            </div>
        </div>
    );
};

// LikePage.propTypes = {
//     dataToLikePage: PropTypes.array,
// };

export default LikePage;
