import React from 'react';
import { mount } from 'enzyme';
import RecoverPassword from '../recover-password/recover-password';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import { baseAxios } from '../../utils';
import mockAdapter from 'axios-mock-adapter';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование всего приложения', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    const component = mount(
        <Provider store={store}>
            <RecoverPassword />
        </Provider>
    );

    it('Тестируем RecoverPassword INPUT-STAGE', async () => {
        // expect.assertions(1);
        expect(component).toMatchSnapshot();

        //user enters email
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'testEmail',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with filled field
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/recover/password',
                {},
                200,
                {
                    message: 'testToNextStage',
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем RecoverPassword CONFIRM-STAGE with incorrect code', async () => {
        //user enters incorrect code
        component.find('input#code-input').simulate('change', {
            target: {
                value: 1111,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with incorrect code
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const responseCode1 = [
            [
                'POST',
                '/recover/password/confirm',
                {},
                400,
                {
                    code: 2,
                },
            ],
        ];
        await multipleRequest(mockAxios, responseCode1);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем RecoverPassword CONFIRM-STAGE with correct code', async () => {
        //user enters correct code
        component.find('input#code-input').simulate('change', {
            target: {
                value: 2222,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with correct code
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const responseCode2 = [
            [
                'POST',
                '/recover/password/confirm',
                {},
                200,
                {
                    message: 'testGoToTheNextStage',
                },
            ],
        ];
        await multipleRequest(mockAxios, responseCode2);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем RecoverPassword FINAL-STAGE', async () => {
        //user enters new invalid password
        component.find('input#password').simulate('change', {
            target: {
                value: 'testPassword',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //user enters new password with three or more same symbols
        component.find('input#password').simulate('change', {
            target: {
                value: 'testPPPPassword',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //user enters new valid password
        component.find('input#password').simulate('change', {
            target: {
                value: 'testPassword*',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //user repeats new valid password but not same
        component.find('input#repeat-password').simulate('change', {
            target: {
                value: 'testMissmatchedPassword*',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //user repeats new valid password
        component.find('input#repeat-password').simulate('change', {
            target: {
                value: 'testPassword*',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with correct code
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/recover/password/final',
                {},
                200,
                {
                    message: 'testSuccess',
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();

        //click "Ok" button in modal window
        component.find('div.modal button').simulate('click');
        component.unmount();
        expect(component).toMatchSnapshot();
    });
});
