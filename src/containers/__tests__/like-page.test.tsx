import React from 'react';
import { mount } from 'enzyme';
import LikePage from '../like-page/like-page';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect } from '@jest/globals';

describe('Тестирование всего приложения', () => {
    it('Тестируем рендер LikePage', () => {
        expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <LikePage />
            </Provider>
        );
        expect(component).toMatchSnapshot();
    });
});
