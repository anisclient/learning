import React, { useState } from 'react';
import { mount } from 'enzyme';
import ProductPage from '../product-page/product-page';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect } from '@jest/globals';
import Header from '../../components/header/header';

describe('Тестирование всего приложения', () => {
    it('Тестируем рендер ProductPage', () => {
        const Test = () => {
            const [burgerActive, setBurgerActive] = useState(false);
            return (
                <>
                    <Header active={burgerActive} setActive={setBurgerActive} />
                    ;
                </>
            );
        };
        // expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <Test />
                <ProductPage />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //click like button
        component.find('button').at(2).simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //click unlike button
        component.find('button').at(2).simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        // //first click add to cart button
        // component.find('button').at(3).simulate('click');
        // component.update();
        // expect(component).toMatchSnapshot();

        // //second click add to cart button
        // component.find('button').at(3).simulate('click');
        // component.update();
        // expect(component).toMatchSnapshot();
    });
});
