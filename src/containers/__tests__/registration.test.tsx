import React from 'react';
import { mount } from 'enzyme';
import { baseAxios } from '../../utils';
import mockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import Registration from '../registration/registration';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect, beforeEach } from '@jest/globals';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование всего приложения', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    const component = mount(
        <Provider store={store}>
            <Registration />
        </Provider>
    );

    it('Тестируем рендер Registration INPUT STAGE', async () => {
        // expect.assertions(1);

        expect(component).toMatchSnapshot();

        //mismatched passwords error
        component.find('input#password').simulate('change', {
            target: {
                value: 'testPassword',
            },
        });
        component.find('input#repeat-password').simulate('change', {
            target: {
                value: 'testRepeatPassword',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //invalid email error
        component.find('input#email').simulate('change', {
            target: {
                value: 'testEmail@mail',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //tree or more two same values error
        component.find('input#password').simulate('change', {
            target: {
                value: 'testTTT',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //user enters login, password, email
        component.find('input#login').simulate('change', {
            target: {
                value: 'testLogin',
            },
        });
        component.find('input#password').simulate('change', {
            target: {
                value: 'testPassword*',
            },
        });
        component.find('input#repeat-password').simulate('change', {
            target: {
                value: 'testPassword*',
            },
        });
        component.find('input#email').simulate('change', {
            target: {
                value: 'testEmail@mail.com',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //register with filled fields
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/register',
                {},
                200,
                {
                    message: 'testRegistration...',
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем Regitration CONFIRM-STAGE with incorrect code', async () => {
        //user enters incorrect code
        component.find('input#code-input').simulate('change', {
            target: {
                value: 1111,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with incorrect code
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const responseCode1 = [
            [
                'POST',
                '/register/confirm',
                {},
                400,
                {
                    code: 2,
                },
            ],
        ];
        await multipleRequest(mockAxios, responseCode1);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем Registration CONFIRM-STAGE with correct code', async () => {
        //user enters correct code
        component.find('input#code-input').simulate('change', {
            target: {
                value: 2222,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with correct code
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const responseCode2 = [
            [
                'POST',
                '/register/confirm',
                {},
                200,
                {
                    message: 'testSuccess',
                },
            ],
        ];
        await multipleRequest(mockAxios, responseCode2);
        component.update();
        expect(component).toMatchSnapshot();

        //click "Ok" button in modal window
        component.find('div.modal button').simulate('click');
        component.update();
        expect(component).toMatchSnapshot();
        component.unmount();
        expect(component).toMatchSnapshot();
    });
});
