import React from 'react';
import { mount } from 'enzyme';
import Login from '../login/login';
import { Provider } from 'react-redux';
import { baseAxios } from '../../utils';
import mockAdapter from 'axios-mock-adapter';
import { store } from '../../__data__/store';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { act } from 'react-dom/test-utils';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование всего приложения', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    it('Тестируем рендер Login', async () => {
        expect.assertions(5);
        //first render
        const component = mount(
            <Provider store={store}>
                <Login />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //TODO
        //empty fields validation
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //user enters login and password
        component.find('input#login-input').simulate('change', {
            target: {
                value: 'testLogin',
            },
        });
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'testPassword',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //login with filled fields
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/login',
                {},
                200,
                {
                    login: 'testLogin',
                    token: 'testToken',
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
