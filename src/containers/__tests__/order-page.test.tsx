import React from 'react';
import { mount } from 'enzyme';
import OrderPage from '../order-page/order-page';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import mockAdapter from 'axios-mock-adapter';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import { baseAxios } from '../../utils';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование всего приложения', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    it('Тестируем рендер OrderPage', async () => {
        // expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <OrderPage />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //user enters name
        component.find('input#name').simulate('change', {
            target: {
                value: 'testName',
            },
        });

        //user enters unvalid phone
        component.find('input#phone').simulate('change', {
            target: {
                value: 1,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //user enters valid phone
        component.find('input#phone').simulate('change', {
            target: {
                value: 79047777777,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //activating textarea
        component
            .find('input#shipping')
            .simulate('change', { input: { checked: true } });

        component.update();
        expect(component).toMatchSnapshot();

        //textarea;
        component.find('textarea.textarea').simulate('change', {
            target: {
                value: 'testAddress',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //continue with filled fields
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/order',
                {},
                200,
                {
                    message: 1,
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();

        //click "Ok" button
        component.find('div.modal button').simulate('click');
        component.update();
        expect(component).toMatchSnapshot();
    });
});
