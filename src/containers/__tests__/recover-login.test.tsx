import React from 'react';
import { mount } from 'enzyme';
import RecoverLogin from '../recover-login/recover-login';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import { baseAxios } from '../../utils';
import mockAdapter from 'axios-mock-adapter';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование RecoverLogin', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    const component = mount(
        <Provider store={store}>
            <RecoverLogin />
        </Provider>
    );

    it('Тестируем рендер RecoverLogin INPUT-STAGE', async () => {
        // expect.assertions(1);
        expect(component).toMatchSnapshot();

        //user enters email
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'testEmail',
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with filled field
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'POST',
                '/recover/login',
                {},
                200,
                {
                    message: 'testToNextStage',
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем RecoverLogin CONFIRM-STAGE with incorrect code', async () => {
        //user enters incorrect code
        component.find('input#code-input').simulate('change', {
            target: {
                value: 1111,
            },
        });

        component.update();
        expect(component).toMatchSnapshot();

        //continue with field filled with incorrect code
        // component.find('form').simulate('submit');
        // component.update();
        // expect(component).toMatchSnapshot();

        // //intercept the request
        // const responseCode1 = [
        //     [
        //         'POST',
        //         '/recover/login/confirm',
        //         {},
        //         400,
        //         {
        //             code: 2,
        //         },
        //     ],
        // ];
        // await multipleRequest(mockAxios, responseCode1);
        // component.update();
        // expect(component).toMatchSnapshot();
    });

    // it('Тестируем RecoverLogin CONFIRM-STAGE with correct code', async () => {
    //     //user enters correct code
    //     component.find('input#code-input').simulate('change', {
    //         target: {
    //             value: 2222,
    //         },
    //     });

    //     component.update();
    //     expect(component).toMatchSnapshot();

    //     //continue with field filled with correct code
    //     component.find('form').simulate('submit');
    //     component.update();
    //     expect(component).toMatchSnapshot();

    //     //intercept the request
    //     const responseCode2 = [
    //         [
    //             'POST',
    //             '/recover/login/confirm',
    //             {},
    //             200,
    //             {
    //                 login: 'testYourLogin',
    //             },
    //         ],
    //     ];
    //     await multipleRequest(mockAxios, responseCode2);
    //     component.update();
    //     expect(component).toMatchSnapshot();
    // });
});
