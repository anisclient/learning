import React, { useState } from 'react';
import { mount } from 'enzyme';
import Main from '../main/main';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import { baseAxios } from '../../utils';
import mockAdapter from 'axios-mock-adapter';
import BasketPage from '../basket-page/basket-page';
import Header from '../../components/header/header';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование всего приложения', () => {
    let mockAxios;

    beforeEach(() => {
        mockAxios = new mockAdapter(baseAxios);
    });

    it('Тестируем рендер Main', async () => {
        const Test = () => {
            const [burgerActive, setBurgerActive] = useState(false);
            return (
                <>
                    <Header active={burgerActive} setActive={setBurgerActive} />
                    ;
                </>
            );
        };
        // expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <Test />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //intercept the request
        const response = [
            [
                'GET',
                '/products',
                {},
                200,
                {
                    products: [
                        {
                            id: 1,
                            title: {
                                ru: "Монитор Samsung C49RG90SSI 48.8'",
                                en: "Monitor Samsung C49RG90SSI 48.8'",
                            },
                            price: 79990,
                            rating: 5,
                            image:
                                'https://avatars.mds.yandex.net/get-mpic/2017291/img_id3470680591314204659.jpeg/orig',
                            liked: false,
                        },
                        {
                            id: 2,
                            title: {
                                ru:
                                    "Наушники накладные bluetooth JBL T460BT черный ('JBLT460BTBLK')",
                                en:
                                    "Wireless On-Ear Headphones JBL T460BT ('JBLT460BTBLK') Black",
                            },
                            price: 2290,
                            rating: 4,
                            image: 'https://img.mvideo.ru/Pdb/50049029b.jpg',
                            liked: false,
                        },
                    ],
                },
            ],
        ];
        await multipleRequest(mockAxios, response);
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Тестируем рендер Main', async () => {
        // expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <Main />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //intercept the request
        // const response = [
        //     [
        //         'GET',
        //         '/products',
        //         {},
        //         200,
        //         {
        //             products: [
        //                 {
        //                     id: 1,
        //                     title: {
        //                         ru: "Монитор Samsung C49RG90SSI 48.8'",
        //                         en: "Monitor Samsung C49RG90SSI 48.8'",
        //                     },
        //                     price: 79990,
        //                     rating: 5,
        //                     image:
        //                         'https://avatars.mds.yandex.net/get-mpic/2017291/img_id3470680591314204659.jpeg/orig',
        //                     liked: false,
        //                 },
        //                 {
        //                     id: 2,
        //                     title: {
        //                         ru:
        //                             "Наушники накладные bluetooth JBL T460BT черный ('JBLT460BTBLK')",
        //                         en:
        //                             "Wireless On-Ear Headphones JBL T460BT ('JBLT460BTBLK') Black",
        //                     },
        //                     price: 2290,
        //                     rating: 4,
        //                     image: 'https://img.mvideo.ru/Pdb/50049029b.jpg',
        //                     liked: false,
        //                 },
        //             ],
        //         },
        //     ],
        // ];
        // await multipleRequest(mockAxios, response);
        // component.update();
        // expect(component).toMatchSnapshot();

        //first click "like" button
        component.find('button.likeButton').first().simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //second click "like" button (to unlike)
        component.find('button.likeButton').first().simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //first click to "add to cart" button
        component
            .find('button.main.green.buttonOffset')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //second click to "add to cart" button
        component
            .find('button.main.green.buttonOffset')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //first click to another "add to cart" button
        component
            .find('button.main.green.buttonOffset')
            .last()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();
    });

    it('Testing BasketPage after adding products to cart', () => {
        const component = mount(
            <Provider store={store}>
                <BasketPage />
            </Provider>
        );
        component.mount();
        expect(component).toMatchSnapshot();

        //increase the product by one
        component
            .find('button.main.basket-products-plus')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //decrease the product by one
        component
            .find('button.main.basket-products-minus')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //remove the product unintentionally
        component
            .find('button.main.basket-products-close')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //click to "Cancel" in modal window
        component.find('div.modal button').last().simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //remove the product
        component
            .find('button.main.basket-products-close')
            .first()
            .simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //click to "Ok" in modal window
        component.find('div.modal button').first().simulate('click');
        component.update();
        expect(component).toMatchSnapshot();
    });
});
