import { Carousel } from '../../components';
import React from 'react';
import style from './style.css';
import { CarouselSlideItem, Images } from '../../components';
import Product from '../../components/product';
import { ProductViewType } from '../../components/product/product';
import { useSelector } from 'react-redux';
import * as selectors from '../../__data__/selectors';
// import { setProducts } from '../../__data__/actions/products';
import {
    useLocalStorageInBasket,
    useLocalStorageToLikeProduct,
} from '../../utils';
/* eslint-disable no-unused-vars */

const slides: CarouselSlideItem[] = [
    {
        slide: Images.Slide1,
        id: 1,
        title: 'loftstylelife',
        link: 'https://loftstylelife.ru/',
    },
    {
        slide: Images.Slide2,
        id: 2,
        title: 'pasabahce',
        link: 'https://www.wildberries.ru/brands/pasabahce',
    },
    {
        slide: Images.Slide3,
        id: 3,
        title: 'liquimoly',
        link: 'https://liquimoly.ru/',
    },
];

const Main: React.FC = () => {
    // const dispatch = useDispatch();

    const isLoaded = useSelector(selectors.allProducts.isLoaded);
    const productsInMain = useSelector(selectors.allProducts.productsInMain);

    useLocalStorageInBasket();
    useLocalStorageToLikeProduct();

    // useEffect(() => {
    //     if (productsInMain && productsInMain.length === 0) {
    //         dispatch(setProducts());
    //     }
    // }, [productsInMain]);

    return (
        <main className={style.main}>
            <div className={style.container}>
                <div className={style.mainBody}>
                    <section className={style.banner}>
                        <Carousel slides={slides} />
                    </section>
                    <section className={style.products}>
                        <div className={style.productsWrapper}>
                            {!isLoaded ? (
                                <p>Loading...</p>
                            ) : (
                                productsInMain &&
                                productsInMain.map((product) => (
                                    <Product
                                        key={product.id}
                                        type={ProductViewType.forMain}
                                        product={product}
                                    />
                                ))
                            )}
                        </div>
                    </section>
                </div>
            </div>
        </main>
    );
};

export default Main;
