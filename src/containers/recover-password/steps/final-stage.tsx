import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { Link, Redirect } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import { useDispatch, useSelector } from 'react-redux';
import { submitRecoverPasswordFinalForm as submit } from '../../../__data__/actions/recover-password-final-stage';
import {
    // setError,
    // setPassword,
    // setRepeatPassword,
    setNeedRedirect,
    reset,
} from '../../../__data__/store/slices/recover-password/final-stage';
// import PropTypes from 'prop-types';
import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import ModalWindow, { ModalWindowType } from '../../../components/modal-window';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

// interface MapStateToProps {
//     password: string;
//     repeatPassword: string;
//     error: string | boolean;
//     needRedirect: boolean;
//     loading: boolean;
// }

interface Values {
    password: string;
    repeatPassword: string;
}

// interface MapDispatchToProps {
//     submit: (data: Data) => void;
//     setPassword: (password: string) => void;
//     setRepeatPassword: (repeatPassword: string) => void;
//     setError: (error: string | boolean) => void;
//     reset: () => void;
// }

// type FinalStageProps = MapStateToProps & MapDispatchToProps;

export const FinalStage: React.FC = () => {
    const loginRef = useRef<HTMLInputElement>();
    const { t } = useTranslation();
    const [modalWindowActive, changeModalWindowActive] = useState(false);
    const [text, setText] = useState('');

    // const password = useSelector(selectors.recoverPassword.finalStage.password);
    // const repeatPassword = useSelector(
    //     selectors.recoverPassword.finalStage.repeatPassword
    // );
    // const error = useSelector(selectors.recoverPassword.finalStage.error);
    // const loading = useSelector(selectors.recoverPassword.finalStage.loading);
    const needRedirect = useSelector(
        selectors.recoverPassword.finalStage.needRedirect
    );

    const dispatch = useDispatch();

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();
        const { password, repeatPassword } = values;

        if (password && password === repeatPassword) {
            return submit({ password }, setText, handleShowPopap, t);
        }
        // else {
        // dispatch(setError('Укажите новый пароль два раза'));
        // if (repeatPassword && password && password !== repeatPassword) {
        // return {
        //     password: 'Пароли не совпадают',
        //     repeatPassword: 'Пароли не совпадают',
        // };
        // dispatch(setError('Пароли не совпадают'));
        // }
        // }
    }

    // function handlePasswordChange(event) {
    //     dispatch(setPassword(event.target.value));
    // }

    // function handleRepeatPasswordChange(event) {
    //     dispatch(setRepeatPassword(event.target.value));
    // }

    useEffect(() => {
        return function () {
            dispatch(reset());
        };
    }, []);

    if (needRedirect) {
        return <Redirect to={URLs.login.url} />;
    }

    function handleShowPopap() {
        changeModalWindowActive(!modalWindowActive);
    }

    function finishRecoverPassword() {
        handleShowPopap();
        dispatch(setNeedRedirect());
    }

    function validateForm(values) {
        interface Errors {
            password?: string;
            repeatPassword?: string;
        }
        const errors: Errors = {};

        if (!values.password) {
            errors.password = t(
                'i18next.recoverpassword.final.errorpasswordisrequired'
            );
        }

        if (!values.repeatPassword) {
            errors.repeatPassword = t(
                'i18next.recoverpassword.final.errorrepeatpasswordisrequired'
            );
        } else if (values.password !== values.repeatPassword) {
            errors.repeatPassword = t(
                'i18next.recoverpassword.final.errormismatchedpasswords'
            );
        }
        if (values.password && !/[!?\-&_*]/.test(values.password)) {
            errors.password = `${t(
                'i18next.recoverpassword.final.erroratleastoneof'
            )} !,?,-,&,_,*`;
        }
        if (values.password && /(.)\1{2}/.test(values.password)) {
            errors.password = t(
                'i18next.recoverpassword.final.erroridenticalsymbols'
            );
        }

        return errors;
    }

    return (
        <>
            {modalWindowActive && (
                <ModalWindow
                    buttonText="Ok"
                    modalText={text}
                    switchShowPopap={handleShowPopap}
                    handler={finishRecoverPassword}
                    type={ModalWindowType.onlyInformationType}
                />
            )}
            <Form
                onSubmit={handleSubmit}
                validate={validateForm}
                subscription={{ submitting: true }}
                render={({ handleSubmit }) => (
                    <form className={style.form} onSubmit={handleSubmit}>
                        <h3 className={style.formTitle}>
                            {t('i18next.recoverpassword.final.title')}
                        </h3>
                        <p className={style.formText}>
                            {t('i18next.recoverpassword.final.informtext')}
                        </p>
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    component={LabeledInput}
                                    name="password"
                                    inputRef={loginRef}
                                    label={t(
                                        'i18next.recoverpassword.final.newpassword'
                                    )}
                                    id="password"
                                    placeholder={t(
                                        'i18next.recoverpassword.final.enternewpassword'
                                    )}
                                    type="password"
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                submitting: true,
                            }}
                            render={({ submitting }) => (
                                <Field
                                    component={LabeledInput}
                                    name="repeatPassword"
                                    label={t(
                                        'i18next.recoverpassword.final.repeatnewpassword'
                                    )}
                                    id="repeat-password"
                                    placeholder={t(
                                        'i18next.recoverpassword.final.enternewpassword'
                                    )}
                                    type="password"
                                    disabled={submitting}
                                />
                            )}
                        />
                        <FormSpy
                            subscription={{
                                pristine: true,
                                errors: true,
                                submitting: true,
                            }}
                            render={({ pristine, errors, submitting }) => (
                                <Button
                                    colorScheme={ButtonColors.green}
                                    className={style.buttonOffset}
                                    type="submit"
                                    disabled={
                                        pristine ||
                                        errors?.password ||
                                        errors?.repeatPassword ||
                                        submitting
                                    }
                                    loading={submitting}
                                >
                                    {t(
                                        'i18next.recoverpassword.final.continue'
                                    )}
                                </Button>
                            )}
                        />

                        <Link to={URLs.login.url} className={style.cancelLink}>
                            {t('i18next.recoverpassword.final.cancel')}
                        </Link>
                    </form>
                )}
            />
        </>
        // <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Востановление пароля</h3>
        //     <p className={style.formText}>Установка нового пароля</p>
        //     <LabeledInput
        //         inputRef={loginRef}
        //         label="Новый пароль:"
        //         id="password"
        //         name="password"
        //         placeholder="Введите новый пароль"
        //         value={password}
        //         onChange={handlePasswordChange}
        //         type="password"
        //         error={error}
        //         disabled={loading}
        //     />
        //     <LabeledInput
        //         label="Повторите новый пароль:"
        //         id="repeat-password"
        //         name="repeat-password"
        //         placeholder="Повторите новый пароль"
        //         value={repeatPassword}
        //         onChange={handleRepeatPasswordChange}
        //         type="password"
        //         error={!!error}
        //         disabled={loading}
        //     />
        //     <Button
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //         disabled={loading}
        //         loading={loading}
        //     >
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.cancelLink}>
        //         Отменить
        //     </Link>
        // </form>
    );
};

// const mapStateToProps = ({
//     recoverPassword: { finalStage },
// }): MapStateToProps => ({
//     password: finalStage.password,
//     repeatPassword: finalStage.repeatPassword,
//     error: finalStage.error,
//     needRedirect: finalStage.needRedirect,
//     loading: finalStage.loading,
// });

// const mapDispatchToProps = {
//     submit: submitRecoverPasswordFinalForm,
//     setPassword,
//     setRepeatPassword,
//     setError,
//     reset,
// };

// FinalStage.propTypes = {
//     password: PropTypes.string.isRequired,
//     repeatPassword: PropTypes.string.isRequired,
//     error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
//     needRedirect: PropTypes.bool.isRequired,
//     loading: PropTypes.bool.isRequired,
//     submit: PropTypes.func.isRequired,
//     setPassword: PropTypes.func.isRequired,
//     setRepeatPassword: PropTypes.func.isRequired,
//     setError: PropTypes.func.isRequired,
//     reset: PropTypes.func.isRequired,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(FinalStage);
