import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { Link } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import { StageProps } from '../recover-password';
import PropTypes from 'prop-types';
import { submitRecoverPasswordConfirmForm as submit } from '../../../__data__/actions/recover-password-confirm-stage';
// import {
// setCode,
// setError,
// } from '../../../__data__/store/reducers/recover-password/confirm-stage';
// import { useDispatch } from 'react-redux';
// import * as selectors from '../../../__data__/selectors';
import { Field, Form, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';
/* eslint-disable no-unused-vars */

// interface MapStateToProps {
//     code: string;
//     error: string | boolean;
//     loading: boolean;
// }

interface Values {
    code: number;
}

// interface MapDispatchToProps {
//     submit: (data: Data, StageProps) => void;
//     setCode: (code: string) => void;
//     setError: (error: string | boolean) => void;
// }

// type ConfirmStageProps = StageProps & MapStateToProps & MapDispatchToProps;

export const ConfirmStage: React.FC<StageProps> = ({
    setStep,
    // code,
    // error,
    // loading,
    // submit,
    // setCode,
    // setError,
}) => {
    const loginRef = useRef<HTMLInputElement>();
    const { t } = useTranslation();

    // const code = useSelector(selectors.recoverPassword.confirmStage.code);
    // const error = useSelector(selectors.recoverPassword.confirmStage.error);
    // const loading = useSelector(selectors.recoverPassword.confirmStage.loading);

    // const dispatch = useDispatch();

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();
        const { code } = values;

        if (code) {
            return submit({ code }, setStep, t);
        } else {
            // dispatch(setError('Укажите проверочный код'));
        }
    }

    // function handleChange(event) {
    //     dispatch(setCode(event.target.value));
    // }

    function validateForm(values) {
        interface Errors {
            code?: string;
        }
        const errors: Errors = {};

        if (!values.code) {
            errors.code = t(
                'i18next.recoverpassword.confirm.errorecodeisrequired'
            );
        }
        return errors;
    }

    return (
        <Form
            onSubmit={handleSubmit}
            subscription={{ submitting: true }}
            validate={validateForm}
            render={({ handleSubmit }) => (
                <form className={style.form} onSubmit={handleSubmit}>
                    <h3 className={style.formTitle}>
                        {t('i18next.recoverpassword.confirm.confirmation')}
                    </h3>
                    <p className={style.formText}>
                        {t('i18next.recoverpassword.confirm.informtext')}
                    </p>
                    <FormSpy
                        subscription={{
                            submitting: true,
                        }}
                        render={({ submitting }) => (
                            <Field
                                name="code"
                                component={LabeledInput}
                                inputRef={loginRef}
                                label={t(
                                    'i18next.recoverpassword.confirm.code'
                                )}
                                id="code-input"
                                placeholder={t(
                                    'i18next.recoverpassword.confirm.entercode'
                                )}
                                type="number"
                                disabled={submitting}
                            />
                        )}
                    />
                    <FormSpy
                        subscription={{
                            pristine: true,
                            submitting: true,
                            errors: true,
                        }}
                        render={({ pristine, submitting, errors }) => (
                            <Button
                                colorScheme={ButtonColors.green}
                                className={style.buttonOffset}
                                type="submit"
                                disabled={
                                    pristine || errors?.code || submitting
                                }
                                loading={submitting}
                            >
                                {t('i18next.recoverpassword.confirm.continue')}
                            </Button>
                        )}
                    />
                    <Link to={URLs.login.url} className={style.cancelLink}>
                        {t('i18next.recoverpassword.confirm.cancel')}
                    </Link>
                </form>
            )}
        />
        // <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Подтверждение</h3>
        //     <p className={style.formText}>
        //         После ввода кода Вам нужно будет сменить пароль (правильный код
        //         - 123)
        //     </p>
        //     <LabeledInput
        //         inputRef={loginRef}
        //         label="Код:"
        //         id="code-input"
        //         name="code"
        //         placeholder="Введите код"
        //         value={code}
        //         onChange={handleChange}
        //         error={error}
        //         type="number"
        //         disabled={loading}
        //     />
        //     <Button
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //         disabled={loading}
        //         loading={loading}
        //     >
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.cancelLink}>
        //         Отменить
        //     </Link>
        // </form>
    );
};

// const mapStateToProps = ({
//     recoverPassword: { confirmStage },
// }): MapStateToProps => ({
//     code: confirmStage.code,
//     error: confirmStage.error,
//     loading: confirmStage.loading,
// });

// const mapDispatchToProps = {
//     submit: submitRecoverPasswordConfirmForm,
//     setCode: setCode,
//     setError: setError,
// };

ConfirmStage.propTypes = {
    setStep: PropTypes.func.isRequired,
    // code: PropTypes.string.isRequired,
    // loading: PropTypes.bool.isRequired,
    // error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
    // submit: PropTypes.func.isRequired,
    // setCode: PropTypes.func.isRequired,
    // setError: PropTypes.func.isRequired,
};

// export default connect(mapStateToProps, mapDispatchToProps)(ConfirmStage);
