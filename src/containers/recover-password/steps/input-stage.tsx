import { LabeledInput, Button, ButtonColors } from '../../../components';
import React, { useEffect, useRef } from 'react';
import style from './style.css';
import { StageProps } from '../recover-password';
import { Link } from 'react-router-dom';
import { URLs } from '../../../__data__/urls';
import PropTypes from 'prop-types';
import {
    submitRecoverPasswordInputForm as submit,
    // emailChangeActionCreator as setEmail,
    // setErrorActionCreator as setError,
} from '../../../__data__/actions/recover-password-input-stage';
// import { useDispatch } from 'react-redux';
// import * as selectors from '../../../__data__/selectors';
import { Form, Field, FormSpy } from 'react-final-form';
import { useTranslation } from 'react-i18next';

/* eslint-disable no-unused-vars */

// interface MapStateToProps {
//     email: string;
//     loading: boolean;
//     error: string | boolean;
// }

interface Values {
    email: string;
}

// interface MapDispatchToProps {
//     submit: (data: Data, StageProps) => void;
//     setEmail: (email: string) => void;
//     setError: (error: string | boolean) => void;
// }

// type InputStageProps = MapStateToProps & MapDispatchToProps & StageProps;

export const InputStage: React.FC<StageProps> = ({
    setStep,
    // email,
    // loading,
    // error,
    // submit,
    // setEmail,
    // setError,
}) => {
    const loginRef = useRef<HTMLInputElement>();
    const { t } = useTranslation();

    // const email = useSelector(selectors.recoverPassword.inputStage.email);
    // const loading = useSelector(selectors.recoverPassword.inputStage.loading);
    // const error = useSelector(selectors.recoverPassword.inputStage.error);
    // const dispatch = useDispatch();

    useEffect(() => {
        loginRef.current && loginRef.current.focus();
    }, [loginRef.current]);

    async function handleSubmit(
        // event: React.FormEvent<HTMLFormElement>
        values: Values
    ) {
        // event.preventDefault();
        const { email } = values;

        if (email) {
            return submit({ email }, setStep, t);
        } else {
            // dispatch(setError('Необходимо указать email'));
        }
    }

    // function handleChange(event) {
    //     dispatch(setEmail(event.target.value));
    // }

    function validateForm(values) {
        interface Errors {
            email?: string;
        }
        const errors: Errors = {};

        if (!values.email) {
            errors.email = t(
                'i18next.recoverpassword.input.erroremailisrequired'
            );
        }
        return errors;
    }

    return (
        <Form
            onSubmit={handleSubmit}
            validate={validateForm}
            subscription={{ submitting: true }}
            render={({ handleSubmit }) => (
                <form className={style.form} onSubmit={handleSubmit}>
                    <h3 className={style.formTitle}>
                        {t('i18next.recoverpassword.input.title')}
                    </h3>
                    <p className={style.formText}>
                        {t('i18next.recoverpassword.input.informtext')}
                    </p>
                    <FormSpy
                        subscription={{
                            submitting: true,
                        }}
                        render={({ submitting }) => (
                            <Field
                                name="email"
                                component={LabeledInput}
                                inputRef={loginRef}
                                label={t(
                                    'i18next.recoverpassword.input.youremail'
                                )}
                                id="email-input"
                                placeholder={t(
                                    'i18next.recoverpassword.input.enteremail'
                                )}
                                disabled={submitting}
                            />
                        )}
                    />
                    {/* <Field
                        name="email"
                        component={LabeledInput}
                        inputRef={loginRef}
                        label="Ваш email:"
                        id="email-input"
                        placeholder="Введите email"
                    /> */}
                    <FormSpy
                        subscription={{
                            pristine: true,
                            errors: true,
                            submitting: true,
                        }}
                        render={({ pristine, errors, submitting }) => {
                            return (
                                <Button
                                    colorScheme={ButtonColors.green}
                                    className={style.buttonOffset}
                                    type="submit"
                                    disabled={
                                        pristine || errors?.email || submitting
                                    }
                                    loading={submitting}
                                >
                                    {t(
                                        'i18next.recoverpassword.input.continue'
                                    )}
                                </Button>
                            );
                        }}
                    />

                    <Link to={URLs.login.url} className={style.cancelLink}>
                        {t('i18next.recoverpassword.input.cancel')}
                    </Link>
                </form>
            )}
        />

        //     <form className={style.form} onSubmit={handleSubmit}>
        //     <h3 className={style.formTitle}>Восстановление пароля</h3>
        //     <p className={style.formText}>
        //         На введенный email придет код подтверждения
        //     </p>
        //     <Field name="email" component={LabeledInput} inputRef={loginRef}
        //         label="Ваш email:"
        //         id="email-input" />
        //     <LabeledInput
        //         inputRef={loginRef}
        //         label="Ваш email:"
        //         id="email-input"
        //         name="email"
        //         placeholder="Введите email"
        //         value={email}
        //         onChange={handleChange}
        //         error={error}
        //         disabled={loading}
        //     />
        //     <Button
        //         colorScheme={ButtonColors.green}
        //         className={style.buttonOffset}
        //         type="submit"
        //         disabled={loading}
        //         loading={loading}
        //     >
        //         Продолжить
        //     </Button>
        //     <Link to={URLs.login.url} className={style.cancelLink}>
        //         Отменить
        //     </Link>
        // </form>
    );
};

// const mapStateToProps = ({
//     recoverPassword: { inputStage },
// }): MapStateToProps => ({
//     email: inputStage.email,
//     loading: inputStage.loading,
//     error: inputStage.error,
// });

// const mapDispatchToProps = {
//     submit: submitRecoverPasswordInputForm,
//     setEmail: emailChangeActionCreator,
//     setError: setErrorActionCreator,
// };

InputStage.propTypes = {
    setStep: PropTypes.func.isRequired,
    // email: PropTypes.string.isRequired,
    // loading: PropTypes.bool.isRequired,
    // error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
    // submit: PropTypes.func.isRequired,
    // setEmail: PropTypes.func.isRequired,
    // setError: PropTypes.func.isRequired,
};

// export default connect(mapStateToProps, mapDispatchToProps)(InputStage);
