import React, { ReactElement, useReducer } from 'react';
import { ConfirmStage } from './steps/confirm-stage';
import { FinalStage } from './steps/final-stage';
import { InputStage } from './steps/input-stage';

/* eslint-disable no-unused-vars */

export interface StageProps {
    setStep: (action: { type: 'next' }) => void;
}

export enum RecoverPasswordStep {
    INPUT_STEP,
    CONFIRM_STEP,
    FINAL_STEP,
}

const steps = {
    [RecoverPasswordStep.INPUT_STEP]: {
        component: InputStage,
        next: RecoverPasswordStep.CONFIRM_STEP,
    },
    [RecoverPasswordStep.CONFIRM_STEP]: {
        component: ConfirmStage,
        next: RecoverPasswordStep.FINAL_STEP,
    },
    [RecoverPasswordStep.FINAL_STEP]: {
        component: FinalStage,
    },
};

function RecoverPassword(): ReactElement {
    const [currentStep, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'next':
                if (steps[state].next) {
                    return steps[state].next;
                }
                return state;
            default:
                return state;
        }
    }, RecoverPasswordStep.INPUT_STEP);

    const Stage: React.FC<StageProps> = steps[currentStep].component;

    return <Stage setStep={dispatch} />;
}

export default RecoverPassword;
