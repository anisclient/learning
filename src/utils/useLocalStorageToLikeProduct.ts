import { likeProduct } from '../__data__/actions/products';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../__data__/selectors';

export const useLocalStorageToLikeProduct = () => {
    const dispatch = useDispatch();
    const likedProductsFromRedux = useSelector(
        selectors.allProducts.likedProducts
    );

    const [likedProducts, setLikedProducts] = useState(
        likedProductsFromRedux.length === 0
            ? window.localStorage['liked-products-in-local-storage'] &&
                  JSON.parse(
                      window.localStorage['liked-products-in-local-storage']
                  )
            : likedProductsFromRedux
    );

    useEffect(() => {
        if (
            likedProductsFromRedux.length === 0 &&
            window.localStorage['liked-products-in-local-storage']
        ) {
            const likedProductsFromLocalStorageArr = JSON.parse(
                window.localStorage['liked-products-in-local-storage']
            );

            likedProductsFromLocalStorageArr.forEach((product) => {
                dispatch(likeProduct(product));
            });
        }
    }, []);

    useEffect(() => {
        if (likedProductsFromRedux) {
            setLikedProducts(likedProductsFromRedux);
            window.localStorage.setItem(
                'liked-products-in-local-storage',
                JSON.stringify(likedProductsFromRedux)
            );
        }
    }, [likedProductsFromRedux]);

    return likedProducts;
};
