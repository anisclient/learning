import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../__data__/store/reducers';
import {
    addToBasketFromLocalStorage,
    AllProducts,
} from '../__data__/store/reducers/basket';

export const useLocalStorageInBasket = () => {
    const dispatch = useDispatch();

    const allProductsFromBasketReducer = useSelector<
        AppStore,
        AllProducts | AllProducts[]
    >(({ basket }) => basket.allProducts);

    const productsByGroup = Object.keys(allProductsFromBasketReducer).map(
        (key) => {
            return allProductsFromBasketReducer[key].productsById[0];
        }
    );

    const [productsInBasket, setProductsInBasket] = useState(
        productsByGroup.length === 0
            ? window.localStorage['products-in-basket-local-storage'] &&
                  JSON.parse(
                      window.localStorage['products-in-basket-local-storage']
                  )
            : allProductsFromBasketReducer
    );

    useEffect(() => {
        if (
            productsByGroup.length === 0 &&
            window.localStorage['products-in-basket-local-storage']
        ) {
            const productsInBasketFromLocalStorage = JSON.parse(
                window.localStorage['products-in-basket-local-storage']
            );

            dispatch(
                addToBasketFromLocalStorage(productsInBasketFromLocalStorage)
            );
        }
    }, []);

    useEffect(() => {
        if (allProductsFromBasketReducer) {
            setProductsInBasket(allProductsFromBasketReducer);
            window.localStorage.setItem(
                'products-in-basket-local-storage',
                JSON.stringify(allProductsFromBasketReducer)
            );
        }
    }, [allProductsFromBasketReducer]);

    return productsInBasket;
};
