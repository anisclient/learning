import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const baseApiUrl = getConfigValue('e_zone.api');

export const baseAxios = axios.create({
    baseURL: baseApiUrl,
    headers: {
        'Content-Type': 'application/json;charset=utf-8',
    },
});
