import React, { StrictMode } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './app.css';
import Dashboard from './containers/dashboard/dashboard';
import { store } from './__data__/store';
import { Provider } from 'react-redux';

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <StrictMode>
                    <Dashboard />
                </StrictMode>
            </BrowserRouter>
        </Provider>
    );
};

export default App;
