import { AppStore } from '../../../__data__/store/reducers';
import { RecoverLoginConfirmStageState } from '../../store/slices/recover-login/confirm-stage';
import { createSelector } from 'reselect';

const rootSelector = createSelector<
    AppStore,
    AppStore,
    RecoverLoginConfirmStageState
>(
    (state) => state,
    (state) => state.recoverLogin.confirmStage
);

// export const code = createSelector(rootSelector, (state) => state.code);
// export const error = createSelector(rootSelector, (state) => state.error);
// export const loading = createSelector(rootSelector, (state) => state.loading);
export const recoveredLogin = createSelector(
    rootSelector,
    (state) => state.recoveredLogin
);
