import { AppStore } from '../../../__data__/store/reducers';
import { RecoverLoginInputStageState } from '../../store/slices/recover-login/input-stage';
import { createSelector } from 'reselect';

const rootSelector = createSelector<
    AppStore,
    AppStore,
    RecoverLoginInputStageState
>(
    (state) => state,
    (state) => state.recoverLogin.inputStage
);

export const data = createSelector(rootSelector, (state) => state.data);
// export const email = createSelector(rootSelector, (state) => state.email);
// export const error = createSelector(rootSelector, (state) => state.error);
// export const loading = createSelector(rootSelector, (state) => state.loading);
