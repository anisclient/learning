import { AppStore } from '../../store/reducers';
import { createSelector } from 'reselect';
import { UserState } from '../../store/slices/user';

const rootSelector = createSelector<AppStore, AppStore, UserState>(
    (state) => state,
    (state) => state.user
);
export const isLogin = createSelector(rootSelector, (state) => state.isLogin);
// export const userName = createSelector(rootSelector, (state) => state.userName);
