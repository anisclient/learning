import { AppStore } from '../../../__data__/store/reducers';
import { ProductsState } from '../../../__data__/store/reducers/products';
import { createSelector } from 'reselect';

const rootSelector = createSelector<AppStore, AppStore, ProductsState>(
    (state) => state,
    (state) => state.products
);

export const isLoaded = createSelector(rootSelector, (state) => state.isLoaded);

export const productsInMain = createSelector(
    rootSelector,
    (state) => state.productsList
);

export const likedProducts = createSelector(rootSelector, (state) =>
    state.productsList.filter((product) => !!product.liked === true)
);

// export const singleProduct = createSelector(
//     rootSelector,
//     (state) => state.singleProductPageProduct
// );
