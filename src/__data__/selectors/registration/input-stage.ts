import { AppStore } from '../../../__data__/store/reducers';
import { createSelector } from 'reselect';
import { RegistrationInputStage } from '../../store/slices/registration/inpus-stage';

const rootSelector = createSelector<AppStore, AppStore, RegistrationInputStage>(
    (state) => state,
    (state) => state.registration.inputStage
);
// const formSelector = createSelector(rootSelector, (state) => state.form);
// const validationErrorsSelector = createSelector(
//     rootSelector,
//     (state) => state.validationErrors
// );

// export const loading = createSelector(rootSelector, (state) => state.loading);
export const data = createSelector(rootSelector, (state) => state.data);
// export const error = createSelector(rootSelector, (state) => state.error);
// export const email = createSelector(formSelector, (state) => state.email);
// export const login = createSelector(formSelector, (state) => state.login);
// export const password = createSelector(formSelector, (state) => state.password);
// export const repeatPassword = createSelector(
//     formSelector,
//     (state) => state.repeatPassword
// );
// export const commonError = createSelector(
//     validationErrorsSelector,
//     (state) => state.common
// );
// export const samePasswordsError = createSelector(
//     validationErrorsSelector,
//     (state) => state.samePasswords
// );
