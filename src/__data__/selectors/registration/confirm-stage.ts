import { AppStore } from '../../../__data__/store/reducers';
import { createSelector } from 'reselect';
import { RegistrationConfirmStage } from '../../store/slices/registration/confirm-stage';

const rootSelector = createSelector<
    AppStore,
    AppStore,
    RegistrationConfirmStage
>(
    (state) => state,
    (state) => state.registration.confirmStage
);

// export const code = createSelector(rootSelector, (state) => state.code);
// export const error = createSelector(rootSelector, (state) => state.error);
export const isReady = createSelector(rootSelector, (state) => state.isReady);
// export const loading = createSelector(rootSelector, (state) => state.loading);
