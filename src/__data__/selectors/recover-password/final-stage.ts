import { AppStore } from '../../../__data__/store/reducers';
import { RecoverPasswordFinalStageState } from '../../store/slices/recover-password/final-stage';
import { createSelector } from 'reselect';

const rootSelector = createSelector<
    AppStore,
    AppStore,
    RecoverPasswordFinalStageState
>(
    (state) => state,
    (state) => state.recoverPassword.finalStage
);

// export const loading = createSelector(rootSelector, (state) => state.loading);
// export const password = createSelector(rootSelector, (state) => state.password);
// export const repeatPassword = createSelector(
//     rootSelector,
//     (state) => state.repeatPassword
// );
// export const error = createSelector(rootSelector, (state) => state.error);
export const needRedirect = createSelector(
    rootSelector,
    (state) => state.needRedirect
);
