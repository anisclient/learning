import * as types from '../action-types';
import { getConfigValue } from '@ijl/cli';
import { createAction } from '@reduxjs/toolkit';
import { baseAxios } from '../../utils';

const submitActionCreator = createAction(types.RECOVER_PASSWORD.INPUT.SUBMIT);

const successActionCreator = createAction(types.RECOVER_PASSWORD.INPUT.SUCCESS);

export const emailChangeActionCreator = createAction<string>(
    types.RECOVER_PASSWORD.INPUT.FORM_EMAIL_CHANGE
);

export const setErrorActionCreator = createAction<string | boolean>(
    types.RECOVER_PASSWORD.INPUT.SET_ERROR
);

export const submitRecoverPasswordInputForm = async ({ email }, setStep, t) => {
    // dispatch(submitActionCreator());
    const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/recover/password', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ email }),
    // });

    // if (response.ok) {
    //     try {
    //         await response.json();
    //         // dispatch(successActionCreator());
    //         setStep({ type: 'next' });
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setErrorActionCreator(error.message));
    //         return { email: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setErrorActionCreator(result.error));
    //         return {
    //             email:
    //                 result.code === 1 &&
    //                 t('i18next.recoverpassword.input.errorcode1'),
    //         };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/recover/password',
            data: { email },
        });
        response?.data?.message && setStep({ type: 'next' });
    } catch (error) {
        return {
            email:
                error?.response?.data?.code === 1 &&
                t('i18next.recoverpassword.input.errorcode1'),
        };
    }
};
