// import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';
// import {
//  setError,
// setNeedRedirect,
// setLoading,
// } from
// '../store/slices/recover-password/final-stage';

export const submitRecoverPasswordFinalForm = async (
    { password },
    setText,
    handleShowPopap,
    t
) => {
    // dispatch(setLoading());
    // const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/recover/password/final', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ password }),
    // });

    // if (response.ok) {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setNeedRedirect());
    //         setText(t('i18next.recoverpassword.final.success'));
    //         handleShowPopap();
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setError(error.message));
    //         return { password: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setError(result.error));
    //         return { password: result.error };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/recover/password/final',
            data: {
                password,
            },
        });

        if (response?.data?.message) {
            setText(t('i18next.recoverpassword.final.success'));
            handleShowPopap();
        }
    } catch (error) {
        return {
            password: error?.response?.data?.error,
        };
    }
};
