// import * as types from '../action-types';
// import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';
import { handleSuccess as successActionCreator } from '../store/slices/recover-login/input-stage';

// const submitActionCreator = () => ({
//     type: types.RECOVER_LOGIN.INPUT.SUBMIT,
// });

// const successActionCreator = (data: unknown) => ({
//     type: types.RECOVER_LOGIN.INPUT.SUCCESS,
//     data,
// });

// export const emailChangeActionCreator = (email: string) => ({
//     type: types.RECOVER_LOGIN.INPUT.FORM_EMAIL_CHANGE,
//     email,
// });

// export const setErrorActionCreator = (error: string | boolean) => ({
//     type: types.RECOVER_LOGIN.INPUT.SET_ERROR,
//     error,
// });

// export const resetDataActionCreator = () => ({
//     type: types.RECOVER_LOGIN.INPUT.SET_RESET,
// });

export const submitRecoverLoginForm = ({ email }, t) => async (dispatch) => {
    // dispatch(submitActionCreator());
    // const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/recover/login', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ email }),
    // });

    // if (response.ok) {
    //     try {
    //         const answer = await response.json();
    //         dispatch(successActionCreator(answer.message));
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setErrorActionCreator(error.message));
    //         return { email: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setErrorActionCreator(result.error));
    //         return {
    //             email:
    //                 result.code === 1 &&
    //                 t('i18next.recoverlogin.input.errorcode1'),
    //         };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/recover/login',
            data: {
                email,
            },
        });
        dispatch(successActionCreator(response.data.message));
    } catch (error) {
        return {
            email:
                error?.response?.data?.code === 1 &&
                t('i18next.recoverlogin.input.errorcode1'),
        };
    }
};
