// import * as types from '../action-types';
// import { getConfigValue } from '@ijl/cli';
import { setIsLogin } from '../store/slices/user';
import { baseAxios } from '../../utils';

// const submitActionCreator = () => ({ type: types.LOGIN.SUBMIT });

// const successActionCreator = () => ({
//     type: types.LOGIN.SUCCESS,
// });

// export const loginChangeActionCreator = (value: string) => ({
//     type: types.LOGIN.FORM_LOGIN_CHANGE,
//     value,
// });
// export const passwordChangeActionCreator = (value: string) => ({
//     type: types.LOGIN.FORM_PASSWORD_CHANGE,
//     value,
// });
// export const setErrorActionCreator = (error: string | boolean) => ({
//     type: types.LOGIN.SET_ERROR,
//     error,
// });

export const submitLoginFormActionCreator = ({ login, password }, t) => async (
    dispatch
) => {
    // dispatch(submitActionCreator());
    // const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/login', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ login, password }),
    // });
    // if (response.ok) {
    //     try {
    //         const result = await response.json();
    //         window.localStorage.setItem(result.login, result.token);
    //         // dispatch(successActionCreator());
    //         dispatch(setIsLogin(true));
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setErrorActionCreator(error.message));
    //         return { login: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(
    //         //     setErrorActionCreator(result.error || 'Неизвестная ошибка')
    //         // );

    //         return {
    //             login:
    //                 (result.code === 1 && t('i18next.login.errorcode1')) ||
    //                 (result.code === 2 && t('i18next.login.errorcode2')) ||
    //                 t('"i18next.login.unknownerror"'),
    //         };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }

    try {
        const result = await baseAxios({
            method: 'POST',
            url: '/login',
            data: { login, password },
        });

        window.localStorage.setItem(result.data.login, 'sometoken');
        // dispatch(successActionCreator());
        dispatch(setIsLogin(true));
    } catch (error) {
        return {
            login:
                (error?.response?.data?.code === 1 &&
                    t('i18next.login.errorcode1')) ||
                (error?.response?.data?.code === 2 &&
                    t('i18next.login.errorcode2')) ||
                t('"i18next.login.unknownerror"'),
        };
    }
};
