import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';
// import {
//     setError,
//     setLoading,
//     setSuccess,
// } from '../store/reducers/recover-password/confirm-stage';

export const submitRecoverPasswordConfirmForm = async (
    { code },
    setStep,
    t
) => {
    // dispatch(setLoading());
    const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/recover/password/confirm', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ code }),
    // });

    // if (response.ok) {
    //     try {
    //         await response.json();
    //         // dispatch(setSuccess());
    //         setStep({ type: 'next' });
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setError(error.message));
    //         return { code: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setError(result.error));
    //         return {
    //             code:
    //                 result.code === 2 &&
    //                 t('i18next.recoverpassword.confirm.errorcode2'),
    //         };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/recover/password/confirm',
            data: {
                code,
            },
        });

        response?.data?.message && setStep({ type: 'next' });
    } catch (error) {
        return {
            code:
                error?.response?.data?.code === 2 &&
                t('i18next.recoverpassword.confirm.errorcode2'),
        };
    }
};
