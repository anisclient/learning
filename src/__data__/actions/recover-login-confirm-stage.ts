// import * as types from '../action-types';
// import { getConfigValue } from '@ijl/cli';
import forge from 'node-forge';
import { baseAxios } from '../../utils';
import { handleSuccess as successActionCreator } from '../store/slices/recover-login/confirm-stage';

// const submitActionCreator = () => ({
//     type: types.RECOVER_LOGIN.CONFIRM.SUBMIT,
// });

// const successActionCreator = (data: unknown) => ({
//     type: types.RECOVER_LOGIN.CONFIRM.SUCCESS,
//     data,
// });

// export const setErrorActionCreator = (error: string | boolean) => ({
//     type: types.RECOVER_LOGIN.CONFIRM.SET_ERROR,
//     error,
// });

// export const setCodeActionCreator = (code: string) => ({
//     type: types.RECOVER_LOGIN.CONFIRM.FORM_CODE_CHANGE,
//     code,
// });

// export const setResetData = () => ({
//     type: types.RECOVER_LOGIN.CONFIRM.SET_RESET,
// });

export const recLoginConfirmFormSubmit = ({ code }, t) => async (dispatch) => {
    // dispatch(submitActionCreator());
    // const baseApiUrl = getConfigValue('e_zone.api');

    return new Promise(function (resolve, reject) {
        const { rsa } = forge.pki;
        rsa.generateKeyPair(
            { bits: 2048, workers: 2 },
            async function (err, keypair) {
                const pubkey = forge.pki.publicKeyToPem(keypair.publicKey);
                try {
                    // const response = await fetch(
                    //     baseApiUrl + '/recover/login/confirm',
                    //     {
                    //         method: 'POST',
                    //         headers: {
                    //             'Content-Type':
                    //                 'application/json;charset=utf-8',
                    //         },
                    //         body: JSON.stringify({
                    //             code,
                    //             pubkey,
                    //         }),
                    //     }
                    // );

                    // if (response.ok) {
                    //     try {
                    //         const result = await response.json();
                    //         const decodedLogin = keypair.privateKey.decrypt(
                    //             forge.util.decode64(result.login),
                    //             'RSA-OAEP'
                    //         );
                    //         // dispatch(successActionCreator(result.login));
                    //         dispatch(successActionCreator(decodedLogin));
                    //         resolve(decodedLogin);
                    //     } catch (error) {
                    //         console.error(error.message);
                    //         // dispatch(setErrorActionCreator(error.message));
                    //         reject({ code: error.message });
                    //     }
                    // } else {
                    //     try {
                    //         const result = await response.json();
                    //         // dispatch(setErrorActionCreator(result.error));
                    //         reject({
                    //             code:
                    //                 result.code === 2 &&
                    //                 t(
                    //                     'i18next.recoverlogin.confirm.errorcode2'
                    //                 ),
                    //         });
                    //     } catch (error) {
                    //         reject({ code: error.message });
                    //     }
                    // }
                    const response = await baseAxios({
                        method: 'POST',
                        url: '/recover/login/confirm',
                        data: {
                            code,
                            pubkey,
                        },
                    });
                    const decodedLogin = keypair.privateKey.decrypt(
                        forge.util.decode64(response?.data?.login),
                        'RSA-OAEP'
                    );
                    // dispatch(successActionCreator(result.login));
                    dispatch(successActionCreator(decodedLogin));
                    resolve(decodedLogin);
                } catch (error) {
                    reject({
                        code:
                            error?.response?.data?.code === 2 &&
                            t('i18next.recoverlogin.confirm.errorcode2'),
                    });
                }
            }
        );
    }).catch((errorObj) => {
        return errorObj;
    });
};
