// import * as types from '../action-types';
import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';

// const submitActionCreator = () => ({ type: types.REGISTRATION.CONFIRM.SUBMIT });
// export const successActionCreator = () => ({
//     type: types.REGISTRATION.CONFIRM.SUCCESS,
// });
// export const errorActionCreator = (payload: string | boolean) => ({
//     type: types.REGISTRATION.CONFIRM.FAILURE,
//     payload,
// });

// export const setIsReadyActionCreator = (payload: boolean) => ({
//     type: types.REGISTRATION.CONFIRM.SET_ISREADY,
//     payload,
// });

// export const formCodeChange = (payload: string) => ({
//     type: types.REGISTRATION.CONFIRM.FORM_CODE_CHANGE,
//     payload,
// });

export const submitCodeForm = async ({ code }, setText, handleShowPopap, t) => {
    // dispatch(submitActionCreator());
    const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/register/confirm', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ code }),
    // });

    // if (response.ok) {
    //     try {
    //         const result = await response.json();
    //         // window.localStorage.setItem(result.login, result.token);
    //         setText(t('i18next.registration.confirm.success'));
    //         handleShowPopap();
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(errorActionCreator(error.message));
    //         return { code: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         const error =
    //             (result.code === 2 &&
    //                 t('i18next.registration.confirm.errorcode2')) ||
    //             t('i18next.registration.confirm.unknownerror');
    //         // dispatch(errorActionCreator(error));
    //         return { code: error };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }

    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/register/confirm',
            data: {
                code,
            },
        });
        if (response?.data?.message) {
            setText(t('i18next.registration.confirm.success'));
            handleShowPopap();
        }
    } catch (error) {
        // dispatch(errorActionCreator(error));
        return {
            code:
                (error?.response?.data?.code === 2 &&
                    t('i18next.registration.confirm.errorcode2')) ||
                t('i18next.registration.confirm.unknownerror'),
        };
    }
};
