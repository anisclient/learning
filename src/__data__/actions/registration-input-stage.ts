import * as types from '../action-types';
// import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';
import { handleSuccess as successActionCreator } from '../store/slices/registration/inpus-stage';
// const submitActionCreator = () => ({ type: types.REGISTRATION.INPUT.SUBMIT });
// const successActionCreator = (payload) => ({
//     type: types.REGISTRATION.INPUT.SUCCESS,
//     payload,
// });
// const errorActionCreator = (payload) => ({
//     type: types.REGISTRATION.INPUT.FAILURE,
//     payload,
// });

// export const setCommonError = (error: string | boolean) => ({
//     type: types.REGISTRATION.INPUT.SET_COMMON_ERROR,
//     error,
// });

// export const setSamePasswordError = (error: string | boolean) => ({
//     type: types.REGISTRATION.INPUT.SET_SAME_PASSWORD_ERROR,
//     error,
// });

// export const formLoginChange = (payload: string) => ({
//     type: types.REGISTRATION.INPUT.FORM_LOGIN_CHANGE,
//     payload,
// });

// export const formEmailChange = (value: string) => ({
//     type: types.REGISTRATION.INPUT.FORM_EMAIL_CHANGE,
//     value,
// });

// export const formPasswordChange = (value: string) => ({
//     type: types.REGISTRATION.INPUT.FORM_PASSWORD_CHANGE,
//     value,
// });
// export const formRepeatPasswordChange = (value: string) => ({
//     type: types.REGISTRATION.INPUT.FORM_REPEAT_PASSWORD_CHANGE,
//     value,
// });

// export const setReset = () => ({
//     type: types.REGISTRATION.INPUT.SET_RESET_DATA,
// });

export const submitRegistrationForm = ({ login, password, email }, t) => async (
    dispatch
) => {
    // dispatch(submitActionCreator());
    // const baseApiUrl = getConfigValue('e_zone.api');
    // const response = await fetch(baseApiUrl + '/register', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({
    //         login,
    //         password,
    //         email,
    //     }),
    // });

    // if (response.ok) {
    //     try {
    //         const answer = await response.json();
    //         dispatch(successActionCreator(answer.message));
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(errorActionCreator(error.message));
    //         return { login: error.message };
    //     }
    // } else {
    //     try {
    //         const result = await response.json();

    //         // const error = result.error ;
    //         // dispatch(errorActionCreator(error));
    //         return {
    //             email:
    //                 result.code === 3 &&
    //                 t('i18next.registration.input.errorcode3'),
    //         };
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'POST',
            url: '/register',
            data: {
                login,
                password,
                email,
            },
        });
        dispatch(successActionCreator(response.data.message));
    } catch (error) {
        return {
            email:
                error?.response?.data?.code === 3 &&
                t('i18next.registration.input.errorcode3'),
        };
    }
};
