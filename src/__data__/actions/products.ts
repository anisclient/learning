import * as types from '../action-types';
import { createAction } from '@reduxjs/toolkit';
import { ProductsItem } from '../store/reducers/products';
// import { getConfigValue } from '@ijl/cli';
import { baseAxios } from '../../utils';

const setPropducts = createAction<ProductsItem[]>(types.PRODUCTS.SET_PRODUCTS);

const setLoaded = createAction<boolean>(types.PRODUCTS.LOADED);

export const likeProduct = createAction<ProductsItem>(
    types.PRODUCTS.LIKE_PRODUCT
);

export const unlikeProduct = createAction<ProductsItem>(
    types.PRODUCTS.UNLIKE_PRODUCT
);

export const chooseProductToSingleProductPage = createAction<ProductsItem>(
    types.PRODUCTS.CHOOSE_PRODUCT
);

export const setProducts = () => async (dispatch) => {
    dispatch(setLoaded(false));

    // const baseApiUrl = getConfigValue('e_zone.api');

    // const response = await fetch(baseApiUrl + '/main', {
    //     method: 'GET',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    // });

    // if (response.ok) {
    //     try {
    //         const result = await response.json();
    //         dispatch(setPropducts(result.products));
    //     } catch (error) {
    //         console.error(error.message);
    //         // dispatch(setErrorActionCreator(error.message));
    //     }
    // } else {
    //     try {
    //         const result = await response.json();
    //         // dispatch(setErrorActionCreator(result.error));
    //     } catch (error) {
    //         console.error(error);
    //     }
    // }
    try {
        const response = await baseAxios({
            method: 'GET',
            url: '/products',
        });
        dispatch(setPropducts(response?.data?.products));
    } catch (error) {
        console.error(error.message);
    }
};
