import { createSlice } from '@reduxjs/toolkit';
import { ProductsItem } from '../products';

export interface AllProducts {
    productsById: ProductsItem[];
    totalPriceForThisGroup: number;
}

export interface BasketState {
    allProducts: AllProducts | AllProducts[];
    totalCount: number;
    totalPrice: number;
}

const initialState: BasketState = {
    allProducts: [
        //     1: {
        //     productsById: [{},{},{}],
        //     totalPriceForThisGroup: 1500,
        //   },
        //      2: {
        //     productsById: [{},{}],
        //     totalPriceForThisGroup: 2000,
        //   },
    ],
    totalCount: 0,
    totalPrice: 0,
};

const toOneDimensionalArray = (array) =>
    [].concat.apply(
        [],
        Object.values(array).map((obj: AllProducts) => obj.productsById)
    );

const slice = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        addToBasket(state, action) {
            const currentProductsWithKeyId = !state.allProducts[
                action.payload.id
            ]
                ? [action.payload]
                : [
                      ...state.allProducts[action.payload.id].productsById,
                      action.payload,
                  ];

            const newAllProducts = {
                ...state.allProducts,
                [action.payload.id]: {
                    productsById: currentProductsWithKeyId,
                    totalPriceForThisGroup: currentProductsWithKeyId.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newAllProducts);

            // const allProductsToCount = [].concat.apply(
            //     [],
            //     Object.values(newAllProducts).map((obj) => obj.productsById)
            // );
            return {
                ...state,
                allProducts: newAllProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
        minusProductById(state, action) {
            const productsInCurrentGroup =
                state.allProducts[action.payload].productsById;

            const productsAfterRemovedOneProductFromGroup =
                productsInCurrentGroup.length > 1
                    ? state.allProducts[action.payload].productsById.slice(1)
                    : productsInCurrentGroup;

            const newProducts = {
                ...state.allProducts,
                [action.payload]: {
                    productsById: productsAfterRemovedOneProductFromGroup,
                    totalPriceForThisGroup: productsAfterRemovedOneProductFromGroup.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newProducts);

            // const allProductsToCount = [].concat.apply(
            //     [],
            //     Object.values(newProducts).map((obj) => obj.productsById)
            // );
            return {
                ...state,
                allProducts: newProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
        plusProductById(state, action) {
            const productsAfterAddedNewProductToGroup = [
                ...state.allProducts[action.payload].productsById,
                state.allProducts[action.payload].productsById[0],
            ];

            const newProducts = {
                ...state.allProducts,
                [action.payload]: {
                    productsById: productsAfterAddedNewProductToGroup,
                    totalPriceForThisGroup: productsAfterAddedNewProductToGroup.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newProducts);

            // const allProductsToCount = [].concat.apply(
            //     [],
            //     Object.values(newProducts).map((obj) => obj.productsById)
            // );
            return {
                ...state,
                allProducts: newProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
        deleteProductsGroup(state, action) {
            const newAllProducts = {
                ...state.allProducts,
            };
            const currentTotalCountForThisGroup =
                newAllProducts[action.payload].productsById.length;
            const currentTotalPriceForThisGroup =
                newAllProducts[action.payload].totalPriceForThisGroup;
            delete newAllProducts[action.payload];
            return {
                ...state,
                allProducts: newAllProducts,
                totalCount: state.totalCount - currentTotalCountForThisGroup,
                totalPrice: state.totalPrice - currentTotalPriceForThisGroup,
            };
        },
        clearBasket(state) {
            state.allProducts = [];
            state.totalCount = 0;
            state.totalPrice = 0;
        },
        addToBasketFromLocalStorage(state, action) {
            const newAllProducts = action.payload;
            const allProductsToCount = toOneDimensionalArray(newAllProducts);

            return {
                ...state,
                allProducts: newAllProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
    },
});

export const reducer = slice.reducer;
export const {
    addToBasket,
    minusProductById,
    plusProductById,
    deleteProductsGroup,
    clearBasket,
    addToBasketFromLocalStorage,
} = slice.actions;
