import * as types from '../../../action-types';

export interface ProductsItem {
    title: {
        ru: string;
        en: string;
    };
    price: number;
    rating: number;
    image: string;
    id: number;
    liked: boolean;
}

export interface ProductsState {
    productsList: ProductsItem[];
    // singleProductPageProduct: ProductsItem;
    isLoaded: boolean;
}

const initialState: ProductsState = {
    productsList: [
        // {
        //     id: 1,
        //     title: 'Монитор Samsung C49RG90SSI 48.8"',
        //     price: 79990,
        //     rating: 5,
        //     image:
        //         'https://avatars.mds.yandex.net/get-mpic/2017291/img_id3470680591314204659.jpeg/orig',
        //     liked: false,
        // },
        // {
        //     id: 2,
        //     title:
        //         'Наушники накладные Bluetooth JBL T460BT Black (JBLT460BTBLK)',
        //     price: 2290,
        //     rating: 4,
        //     image: 'https://img.mvideo.ru/Pdb/50049029b.jpg',
        //     liked: false,
        // },
        // {
        //     id: 3,
        //     title: 'Планшет Apple iPad Pro 12.9 (2020) 128Gb Wi-Fi + Cellular',
        //     price: 100990,
        //     rating: 5,
        //     image:
        //         'https://avatars.mds.yandex.net/get-mpic/1600461/img_id8016826526506802319.jpeg/orig',
        //     liked: false,
        // },
        // {
        //     id: 4,
        //     title: 'Умная колонка Яндекс.Станция Макс',
        //     price: 16990,
        //     rating: 5,
        //     image:
        //         'https://avatars.mds.yandex.net/get-mpic/2008488/img_id3523756470523408791.png/orig',
        //     liked: false,
        // },
        // {
        //     id: 5,
        //     title: 'Ноутбук MSI GL75 Leopard 10SDK',
        //     price: 106000,
        //     rating: 4,
        //     image:
        //         'https://avatars.mds.yandex.net/get-mpic/1911047/img_id3927737409046384292.png/orig',
        //     liked: false,
        // },
        // {
        //     id: 6,
        //     title: 'Умные часы Amazfit Bip',
        //     price: 3990,
        //     rating: 5,
        //     image:
        //         'https://avatars.mds.yandex.net/get-mpic/1581127/img_id6463596931723647265.jpeg/orig',
        //     liked: false,
        // },
    ],
    // singleProductPageProduct: {
    //     id: null,
    //     title: {
    //         ru: '',
    //         en: '',
    //     },
    //     price: null,
    //     rating: null,
    //     image: '',
    //     liked: false,
    // },
    isLoaded: false,
};

const setProducts = (state, action) => ({
    ...state,
    productsList: action.payload,
    isLoaded: true,
});

const setLoaded = (state, action) => ({
    ...state,
    isLoaded: action.payload,
});

const likeProduct = (state, action) => {
    const currentProduct = state.productsList.find(
        (product) => product.id === action.payload.id
    );
    return {
        ...state,
        productsList: state.productsList.map((product) => {
            if (product.id === action.payload.id) {
                return { ...product, liked: true };
            }
            return product;
        }),
        // singleProductPageProduct: { ...currentProduct, liked: true },
    };
};

const unlikeProduct = (state, action) => {
    const currentProduct = state.productsList.find(
        (product) => product.id === action.payload.id
    );
    return {
        ...state,
        productsList: state.productsList.map((product) => {
            if (product.id === action.payload.id) {
                return { ...product, liked: false };
            }
            return product;
        }),
        // singleProductPageProduct: { ...currentProduct, liked: false },
    };
};

// const chooseProductToSingleProductPage = (state, action) => ({
//     ...state,
//     singleProductPageProduct: state.productsList.find(
//         (product) => product.id === action.payload.id
//     ),
// });

const handlers = {
    [types.PRODUCTS.LOADED]: setLoaded,
    [types.PRODUCTS.LIKE_PRODUCT]: likeProduct,
    [types.PRODUCTS.UNLIKE_PRODUCT]: unlikeProduct,
    // [types.PRODUCTS.CHOOSE_PRODUCT]: chooseProductToSingleProductPage,
    [types.PRODUCTS.SET_PRODUCTS]: setProducts,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
