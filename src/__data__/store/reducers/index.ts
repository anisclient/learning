import { combineReducers } from 'redux';
import {
    reducer as registrationInputStageReducer,
    RegistrationInputStage,
} from '../slices/registration/inpus-stage';
import {
    RegistrationConfirmStage,
    reducer as registrationConfirmStageReducer,
} from '../slices/registration/confirm-stage';
// import { reducer as loginReducer, LoginState } from '../slices/login';
import {
    reducer as recoverLoginInputStageReducer,
    RecoverLoginInputStageState,
} from '../slices/recover-login/input-stage';
import {
    reducer as recoverLoginConfirmStageReducer,
    RecoverLoginConfirmStageState,
} from '../slices/recover-login/confirm-stage';
// import recoverPasswordInputStageReducer, {
//     RecoverPasswordInputStage,
// } from '../slices/recover-password/input-stage';
// import {
//     RecoverPasswordConfirmStageState,
//     reducer as recoverPasswordConfirmStageReducer,
// } from '../slices/recover-password/confirm-stage';
import {
    RecoverPasswordFinalStageState,
    reducer as recoverPasswordFinalStageReducer,
} from '../slices/recover-password/final-stage';
import { UserState, reducer as userReducer } from '../slices/user';
import productsReducer, { ProductsState } from './products';
import { reducer as basketReducer, BasketState } from './basket';

export type AppStore = {
    registration: {
        inputStage: RegistrationInputStage;
        confirmStage: RegistrationConfirmStage;
    };
    // login: LoginState;
    recoverLogin: {
        inputStage: RecoverLoginInputStageState;
        confirmStage: RecoverLoginConfirmStageState;
    };
    recoverPassword: {
        // inputStage: RecoverPasswordInputStage;
        // confirmStage: RecoverPasswordConfirmStageState;
        finalStage: RecoverPasswordFinalStageState;
    };
    user: UserState;
    products: ProductsState;
    basket: BasketState;
};

export default combineReducers<AppStore>({
    registration: combineReducers({
        inputStage: registrationInputStageReducer,
        confirmStage: registrationConfirmStageReducer,
    }),
    // login: loginReducer,
    recoverLogin: combineReducers({
        inputStage: recoverLoginInputStageReducer,
        confirmStage: recoverLoginConfirmStageReducer,
    }),
    recoverPassword: combineReducers({
        // inputStage: recoverPasswordInputStageReducer,
        // confirmStage: recoverPasswordConfirmStageReducer,
        finalStage: recoverPasswordFinalStageReducer,
    }),
    user: userReducer,
    products: productsReducer,
    basket: basketReducer,
});
