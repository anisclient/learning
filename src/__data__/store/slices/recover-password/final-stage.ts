import { createSlice } from '@reduxjs/toolkit';

export interface RecoverPasswordFinalStageState {
    // password: string;
    // repeatPassword: string;
    // error: string | boolean;
    needRedirect: boolean;
    // loading: boolean;
}

const initialState: RecoverPasswordFinalStageState = {
    // password: '',
    // repeatPassword: '',
    // error: false,
    needRedirect: false,
    // loading: false,
};

const slice = createSlice({
    name: 'recover-password-final',
    initialState,
    reducers: {
        // setPassword(state, action) {
        //     state.password = action.payload;
        //     state.error = false;
        // },
        // setRepeatPassword(state, action) {
        //     state.repeatPassword = action.payload;
        //     state.error = false;
        // },
        // setError(state, action) {
        //     state.error = action.payload;
        //     state.loading = false;
        // },
        setNeedRedirect(state) {
            state.needRedirect = true;
        },
        // setLoading(state) {
        //     state.loading = true;
        // },
        reset(state) {
            // state.password = initialState.password;
            // state.repeatPassword = initialState.repeatPassword;
            // state.error = initialState.error;
            state.needRedirect = initialState.needRedirect;
            // state.loading = initialState.loading;
        },
    },
});

export const {
    // setPassword,
    // setRepeatPassword,
    // setError,
    setNeedRedirect,
    // setLoading,
    reset,
} = slice.actions;
export const reducer = slice.reducer;
