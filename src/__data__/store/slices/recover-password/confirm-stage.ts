// import { createSlice } from '@reduxjs/toolkit';

// export interface RecoverPasswordConfirmStageState {
//     code: string;
//     error: string | boolean;
//     loading: boolean;
// }

// const initialState: RecoverPasswordConfirmStageState = {
//     code: '',
//     error: false,
//     loading: false,
// };

// const slice = createSlice({
//     name: 'recover-password-confirm',
//     initialState,
//     reducers: {
//         setLoading(state) {
//             state.loading = true;
//         },
//         setError(state, action) {
//             state.error = action.payload;
//             state.loading = false;
//         },
//         setCode(state, action) {
//             state.code = action.payload;
//             state.error = false;
//         },
//         setSuccess(state) {
//             state.code = initialState.code;
//             state.error = initialState.error;
//             state.loading = initialState.loading;
//         },
//     },
// });

// export const { setLoading, setError, setCode, setSuccess } = slice.actions;
// export const reducer = slice.reducer;
