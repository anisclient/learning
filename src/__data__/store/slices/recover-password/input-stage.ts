// import { createSlice } from '@reduxjs/toolkit';

// export interface RecoverPasswordInputStage {
//     email: string;
//     loading: boolean;
//     error: boolean | string;
// }

// const initialState: RecoverPasswordInputStage = {
//     email: '',
//     loading: false,
//     error: false,
// };

// const slice = createSlice({
//     initialState,
//     name: 'recover-password-input',
//     reducers: {
//         handleSubmit(state, action) {
//             state.loading = true;
//         },
//         handleSuccess(state, action) {
//             state.email = initialState.email;
//             state.error = initialState.error;
//             state.loading = initialState.loading;
//         },
//         handleEmailChange(state, action) {
//             state.error = false;
//             state.email = action.payload;
//         },
//         handleError(state, action) {
//             state.loading = false;
//             state.error = action.payload;
//         },
//     },
// });

// export const {
//     handleSubmit,
//     handleSuccess,
//     handleEmailChange,
//     handleError,
// } = slice.actions;
// export const reducer = slice.reducer;
