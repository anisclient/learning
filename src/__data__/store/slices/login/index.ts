// // import * as types from '../../../action-types';
// import { createSlice } from '@reduxjs/toolkit';

// export interface LoginState {
//     login: string;
//     password: string;
//     error: string | boolean;
//     isReady: boolean;
//     loading: boolean;
// }

// const initialState: LoginState = {
//     login: '',
//     password: '',
//     error: false,
//     isReady: false,
//     loading: false,
// };

// // const handleFormChange = (fieldName) => (state, action) => ({
// //     ...state,
// //     [fieldName]: action.value,
// //     error: false,
// // });

// // const handleSetError = (state, action) => ({
// //     ...state,
// //     error: action.error,
// //     loading: false,
// // });

// // const handleSubmit = (state, action) => ({
// //     ...state,
// //     loading: true,
// // });

// // const handleSuccess = (state, action) => ({
// //     ...initialState,
// //     isReady: true,
// // });

// // const handlers = {
// //     [types.LOGIN.FORM_LOGIN_CHANGE]: handleFormChange('login'),
// //     [types.LOGIN.FORM_PASSWORD_CHANGE]: handleFormChange('password'),
// //     [types.LOGIN.SET_ERROR]: handleSetError,
// //     [types.LOGIN.SUBMIT]: handleSubmit,
// //     [types.LOGIN.SUCCESS]: handleSuccess,
// // };

// // export default function (state = initialState, action) {
// //     return handlers[action.type] ? handlers[action.type](state, action) : state;
// // }
// const slice = createSlice({
//     initialState,
//     name: 'login',
//     reducers: {
//         handleFormChangeLogin(state, action) {
//             state.login = action.payload;
//             state.error = false;
//         },
//         handleSetError(state, action) {
//             state.error = action.payload;
//             state.loading = false;
//         },
//         handleSubmit(state) {
//             state.loading = true;
//         },
//         handleSuccess(state) {
//             state.login = initialState.login;
//             state.password = initialState.password;
//             state.error = initialState.error;
//             state.loading = initialState.loading;
//             state.isReady = true;
//         },
//     },
// });

// export const {
//     handleFormChangeLogin,
//     handleSetError,
//     handleSubmit,
//     handleSuccess,
// } = slice.actions;

// export const reducer = slice.reducer;
