// import * as types from '../../../action-types';

import { createSlice } from '@reduxjs/toolkit';

export interface RecoverLoginInputStageState {
    // email: string;
    // error: string | boolean;
    // loading: boolean;
    data: unknown;
}

const initialState: RecoverLoginInputStageState = {
    // email: '',
    // error: false,
    // loading: false,
    data: null,
};

// const handleEmailChange = (state, action) => ({
//     ...state,
//     email: action.email,
//     error: false,
// });

// const handleSetError = (state, action) => ({
//     ...state,
//     error: action.error,
// });

// const handleSubmit = (state) => ({
//     ...state,
//     loading: true,
//     error: false,
// });

// const handleSuccess = (state, action) => ({
//     ...state,
//     error: false,
//     loading: false,
//     data: action.data,
// });

// const handleResetData = () => ({
//     ...initialState,
// });

// const handlers = {
//     [types.RECOVER_LOGIN.INPUT.FORM_EMAIL_CHANGE]: handleEmailChange,
//     [types.RECOVER_LOGIN.INPUT.SET_ERROR]: handleSetError,
//     [types.RECOVER_LOGIN.INPUT.SUCCESS]: handleSuccess,
//     [types.RECOVER_LOGIN.INPUT.SUBMIT]: handleSubmit,
//     [types.RECOVER_LOGIN.INPUT.SET_RESET]: handleResetData,
// };

// export default function (state = initialState, action) {
//     return handlers[action.type] ? handlers[action.type](state, action) : state;
// }

const slice = createSlice({
    initialState,
    name: 'recover-login-input',
    reducers: {
        // handleEmailChange(state, action) {
        //     state.email = action.payload;
        //     state.error = false;
        // },
        // handleSetError(state, action) {
        //     state.error = action.payload;
        // },
        // handleSubmit(state) {
        //     state.loading = true;
        //     state.error = false;
        // },
        handleSuccess(state, action) {
            // state.error = false;
            // state.loading = false;
            state.data = action.payload;
        },
        handleResetData(state) {
            // state.email = initialState.email;
            // state.error = initialState.error;
            // state.loading = initialState.loading;
            state.data = initialState.data;
        },
    },
});

export const {
    // handleEmailChange,
    // handleSubmit,
    // handleSetError,
    handleSuccess,
    handleResetData,
} = slice.actions;

export const reducer = slice.reducer;
