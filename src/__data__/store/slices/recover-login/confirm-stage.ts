// import * as types from '../../../action-types';
import { createSlice } from '@reduxjs/toolkit';

export interface RecoverLoginConfirmStageState {
    // code: string;
    // error: boolean | string;
    recoveredLogin: string;
    // loading: boolean;
}

const initialState: RecoverLoginConfirmStageState = {
    // code: '',
    // error: false,
    recoveredLogin: '',
    // loading: false,
};

// const handleSubmit = (state, action) => ({
//     ...state,
//     loading: true,
// });

// const handleError = (state, action) => ({
//     ...state,
//     loading: false,
//     error: action.error,
// });

// const handleSuccess = (state, action) => ({
//     ...initialState,
//     recoveredLogin: action.data,
// });

// const handleCodeChange = (state, action) => ({
//     ...state,
//     error: false,
//     code: action.code,
// });

// const handleResetData = (state, action) => ({
//     ...state,
//     recoveredLogin: '',
// });

// const handlers = {
//     [types.RECOVER_LOGIN.CONFIRM.SUBMIT]: handleSubmit,
//     [types.RECOVER_LOGIN.CONFIRM.SET_ERROR]: handleError,
//     [types.RECOVER_LOGIN.CONFIRM.SUCCESS]: handleSuccess,
//     [types.RECOVER_LOGIN.CONFIRM.FORM_CODE_CHANGE]: handleCodeChange,
//     [types.RECOVER_LOGIN.CONFIRM.SET_RESET]: handleResetData,
// };

// export default function (state = initialState, action) {
//     return handlers[action.type] ? handlers[action.type](state, action) : state;
// }

const slice = createSlice({
    initialState,
    name: 'recover-login-confirm',
    reducers: {
        // handleSubmit(state) {
        //     state.loading = true;
        // },

        // handleError(state, action) {
        //     state.loading = false;
        //     state.error = action.payload;
        // },
        handleSuccess(state, action) {
            // state.code = initialState.code;
            // state.error = initialState.error;
            // state.loading = initialState.loading;
            state.recoveredLogin = action.payload;
        },
        // handleCodeChange(state, action) {
        //     state.error = false;
        //     state.code = action.payload;
        // },
        handleResetData(state) {
            // state.code = initialState.code;
            // state.error = initialState.error;
            // state.loading = initialState.loading;
            state.recoveredLogin = '';
        },
    },
});

export const {
    // handleSubmit,
    // handleError,
    handleSuccess,
    // handleCodeChange,
    handleResetData,
} = slice.actions;

export const reducer = slice.reducer;
