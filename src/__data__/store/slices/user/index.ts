import { createSlice } from '@reduxjs/toolkit';

export interface UserState {
    isLogin: boolean;
    // userName: string;
}

const initialState: UserState = {
    isLogin: false,
    // userName: '',
};

const slice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setIsLogin(state, action) {
            state.isLogin = action.payload;
        },
        // setUserName(state, action) {
        //     state.userName = action.payload;
        // },
    },
});

export const {
    setIsLogin,
    // setUserName
} = slice.actions;
export const reducer = slice.reducer;
