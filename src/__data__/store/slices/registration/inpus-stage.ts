import { createSlice } from '@reduxjs/toolkit';
// import * as types from '../../../action-types';

type ValidationErrors = {
    common: boolean | string;
    samePasswords: boolean | string;
};

type RegistrationInputStageForm = {
    email: string;
    password: string;
    repeatPassword: string;
    login: string;
};

export interface RegistrationInputStage {
    loading: boolean;
    data: unknown;
    error: boolean | string;
    validationErrors: ValidationErrors;
    form: RegistrationInputStageForm;
}

const initialState: RegistrationInputStage = {
    loading: false,
    data: null,
    error: false,
    validationErrors: {
        common: false,
        samePasswords: false,
    },
    form: {
        email: '',
        password: '',
        repeatPassword: '',
        login: '',
    },
};

// const handleSubmit = (state, action) => ({
//     ...state,
//     loading: true,
//     error: false,
//     form: {
//         ...state.form,
//     },
// });

// const handleSuccess = (state, action) => ({
//     ...initialState,
//     form: { ...initialState.form },
//     data: action.data,
// });

// const handleFailure = (state, action) => ({
//     ...state,
//     loading: false,
//     error: action.error,
// });

// const handleFormChange = (fieldName: string) => (state, action) => ({
//     ...state,
//     form: {
//         ...state.form,
//         [fieldName]: action.value,
//     },
//     error: false,
//     validationErrors: {
//         common: false,
//         samePasswords: false,
//     },
// });

// const handleSetError = (errorName: string) => (state, action) => ({
//     ...state,
//     form: { ...state.form },
//     validationErrors: {
//         ...state.validationErrors,
//         [errorName]: action.error,
//     },
// });

// const handleResetData = (state, action) => ({
//     ...initialState,
//     form: { ...initialState.form },
//     validationErrors: { ...initialState.validationErrors },
// });

// const handlers = {
//     [types.REGISTRATION.INPUT.SUBMIT]: handleSubmit,
//     [types.REGISTRATION.INPUT.SUCCESS]: handleSuccess,
//     [types.REGISTRATION.INPUT.FAILURE]: handleFailure,
//     [types.REGISTRATION.INPUT.FORM_LOGIN_CHANGE]: handleFormChange('login'),
//     [types.REGISTRATION.INPUT.FORM_EMAIL_CHANGE]: handleFormChange('email'),
//     [types.REGISTRATION.INPUT.FORM_PASSWORD_CHANGE]: handleFormChange(
//         'password'
//     ),
//     [types.REGISTRATION.INPUT.FORM_REPEAT_PASSWORD_CHANGE]: handleFormChange(
//         'repeatPassword'
//     ),
//     [types.REGISTRATION.INPUT.SET_COMMON_ERROR]: handleSetError('common'),
//     [types.REGISTRATION.INPUT.SET_SAME_PASSWORD_ERROR]: handleSetError(
//         'samePasswords'
//     ),
//     [types.REGISTRATION.INPUT.SET_RESET_DATA]: handleResetData,
// };

// export default function (state = initialState, action) {
//     return handlers[action.type] ? handlers[action.type](state, action) : state;
// }

const slice = createSlice({
    initialState,
    name: 'registration-input',
    reducers: {
        // handleSubmit(state) {
        //     state.loading = true;
        //     state.error = false;
        //     state.form.email = initialState.form.email;
        //     state.form.password = initialState.form.password;
        //     state.form.repeatPassword = initialState.form.repeatPassword;
        //     state.form.login = initialState.form.login;
        // },
        handleSuccess(state, action) {
            state.loading = initialState.loading;
            state.data = action.payload;
            state.error = initialState.error;
            state.validationErrors.common =
                initialState.validationErrors.common;
            state.validationErrors.samePasswords =
                initialState.validationErrors.samePasswords;
            state.form.email = initialState.form.email;
            state.form.password = initialState.form.password;
            state.form.repeatPassword = initialState.form.repeatPassword;
            state.form.login = initialState.form.login;
        },
        // handleFailure(state, action) {
        //     state.loading = false;
        //     state.error = action.payload;
        // },
        // handleFormChangelogin(state, action) {
        //     state.form.login = action.payload;
        //     state.error = false;
        //     state.validationErrors.common = false;
        //     state.validationErrors.samePasswords = false;
        // },
        // handleFormChangeEmail(state, action) {
        //     state.form.email = action.payload;
        //     state.error = false;
        //     state.validationErrors.common = false;
        //     state.validationErrors.samePasswords = false;
        // },
        // handleFormChangePassword(state, action) {
        //     state.form.password = action.payload;
        //     state.error = false;
        //     state.validationErrors.common = false;
        //     state.validationErrors.samePasswords = false;
        // },
        // handleFormChangeRepeatPassword(state, action) {
        //     state.form.repeatPassword = action.payload;
        //     state.error = false;
        //     state.validationErrors.common = false;
        //     state.validationErrors.samePasswords = false;
        // },
        // handleSetCommonError(state, action) {
        //     state.validationErrors.common = action.payload;
        // },
        // handleSetSamePasswordsError(state, action) {
        //     state.validationErrors.samePasswords = action.payload;
        // },
        handleResetData(state) {
            state.loading = initialState.loading;
            state.data = initialState.data;
            state.error = initialState.error;
            state.validationErrors.common =
                initialState.validationErrors.common;
            state.validationErrors.samePasswords =
                initialState.validationErrors.samePasswords;
            state.form.email = initialState.form.email;
            state.form.password = initialState.form.password;
            state.form.repeatPassword = initialState.form.repeatPassword;
            state.form.login = initialState.form.login;
        },
    },
});

export const {
    // handleSubmit,
    handleSuccess,
    // handleFailure,
    // handleFormChangelogin,
    // handleFormChangeEmail,
    // handleFormChangePassword,
    // handleFormChangeRepeatPassword,
    // handleSetCommonError,
    // handleSetSamePasswordsError,
    handleResetData,
} = slice.actions;

export const reducer = slice.reducer;
