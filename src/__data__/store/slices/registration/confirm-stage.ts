// import * as types from '../../../action-types';
import { createSlice } from '@reduxjs/toolkit';

export type RegistrationConfirmStage = {
    // loading: boolean;
    // error: boolean | string;
    isReady: boolean;
    // code: string;
};

const initialState: RegistrationConfirmStage = {
    // loading: false,
    // error: false,
    isReady: false,
    // code: '',
};

// const handleSubmit = (state, action) => ({
//     ...state,
//     loading: true,
//     error: false,
// });

// const handleSuccess = (state, action) => ({
//     ...initialState,
//     isReady: true,
// });

// const handleFailure = (state, action) => ({
//     ...state,
//     loading: false,
//     error: action.error,
// });

// const handleFormChange = (state, action) => ({
//     ...state,
//     error: false,
//     code: action.code,
// });

// const handleIsReady = (state, action) => ({
//     ...initialState,
// });

// const handlers = {
//     [types.REGISTRATION.CONFIRM.SUBMIT]: handleSubmit,
//     [types.REGISTRATION.CONFIRM.SUCCESS]: handleSuccess,
//     [types.REGISTRATION.CONFIRM.FAILURE]: handleFailure,
//     [types.REGISTRATION.CONFIRM.FORM_CODE_CHANGE]: handleFormChange,
//     [types.REGISTRATION.CONFIRM.SET_ISREADY]: handleIsReady,
// };

// export default function (state = initialState, action) {
//     return handlers[action.type] ? handlers[action.type](state, action) : state;
// }

const slice = createSlice({
    name: 'registration-confirm',
    initialState,
    reducers: {
        // handleSubmit(state, action) {
        //     state.loading = true;
        //     state.error = false;
        // },
        handleSuccess(state) {
            // state.loading = initialState.loading;
            // state.error = initialState.error;
            state.isReady = true;
            // state.code = initialState.code;
        },
        // handleFailure(state, action) {
        //     state.loading = false;
        //     state.error = action.payload;
        // },
        // handleFormChange(state, action) {
        //     state.error = false;
        //     state.code = action.payload;
        // },
        handleIsReady(state, action) {
            // state.loading = initialState.loading;
            // state.error = initialState.error;
            state.isReady = action.payload;
            // state.code = initialState.code;
        },
    },
});

export const {
    // handleSubmit,
    handleSuccess,
    // handleFailure,
    // handleFormChange,
    handleIsReady,
} = slice.actions;

export const reducer = slice.reducer;
