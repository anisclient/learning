import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('e_zone');
export const baseUrl = navigations['e_zone'];

export const URLs = {
    root: {
        url: navigations['e_zone'],
    },
    main: {
        url: navigations['link.e_zone.main'],
    },
    login: {
        url: navigations['link.e_zone.login'],
    },
    basket: {
        url: navigations['link.e_zone.basket'],
    },
    like: {
        url: navigations['link.e_zone.like'],
    },
    register: {
        url: navigations['link.e_zone.register'],
    },
    recoverLogin: {
        url: navigations['link.e_zone.recover.login'],
    },
    recoverPassword: {
        url: navigations['link.e_zone.recover.password'],
    },
    product: {
        url: navigations['link.e_zone.product'],
    },
    order: {
        url: navigations['link.e_zone.order'],
    },
};
