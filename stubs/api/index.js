// const express = require('express');
// const bodyParser = require('body-parser');
const router = require('express').Router();
const nodemailer = require('nodemailer');
const { google } = require('googleapis');
const forge = require('node-forge');
const _ = require('lodash');
const dotenv = require('dotenv');
const bpkdf2Password = require('pbkdf2-password');

const hash = bpkdf2Password();

dotenv.config();

// const app = express();
// app.use(bodyParser.json({ limit: '2mb' }));

const users = [];
const orders = [];
let newUser, currentUser, objIndex, codeToSend;

const wait = (time = 1500) => (req, res, next) => {
    setTimeout(() => {
        next();
    }, time);
};

////mail////
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URL = process.env.REDIRECT_URL;
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;
const USER_EMAIL = process.env.USER_EMAIL;

const oAuth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URL
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

async function sendCodeToMail(mail) {
    try {
        const code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
        codeToSend = code;

        const accessToken = await oAuth2Client.getAccessToken();

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: USER_EMAIL,
                clientId: CLIENT_ID,
                clientSecret: CLIENT_SECRET,
                refreshToken: REFRESH_TOKEN,
                accessToken: accessToken,
            },
        });

        const options = {
            from: 'E_ZONE <almiev.anis@gmail.com>',
            to: mail,
            subject: 'E_ZONE ✔',
            text: 'Код подтверждения',
            html: `<p>Код подтверждения: <b>${codeToSend}</b></p>`,
        };

        const result = await transporter.sendMail(options);
        return result;
    } catch (error) {
        return error;
    }
}
////////

router.post('/register', wait(), (req, res) => {
    const { email, login, password } = req.body;
    const exist = users.find((u) => u.email === email);

    if (exist) {
        res.status(400).send({
            error: 'This email is already registered!',
            code: 3,
        });
    } else {
        hash({ password }, (err, pass, salt, hash) => {
            if (err) throw err;

            newUser = {
                id: _.uniqueId(),
                email,
                login,
                hash,
                salt,
            };

            sendCodeToMail(newUser.email);
            res.send({
                message:
                    'Registration. A code has been sent to the entered email',
            });
        });
    }
});

router.post('/register/confirm', wait(), (req, res) => {
    const { code } = req.body;

    if (codeToSend === Number(code)) {
        users.push(newUser);
        newUser = '';
        res.send({
            message: 'Registration completed successfully',
        });
    } else {
        res.status(400).send({ error: 'Incorrect code', code: 2 });
    }
});

router.post('/login', wait(), (req, res) => {
    const { login, password } = req.body;
    const user = users.find((u) => u.login === login);

    if (user) {
        hash({ password, salt: user.salt }, (err, pass, salt, hash) => {
            if (err) throw err;

            if (hash === user.hash) {
                const { salt, hash, ...rest } = user;

                res.send({
                    login: rest.login,
                    // token: ' ---> токен <---',
                });
            } else {
                res.status(400).send({
                    error: 'Incorrect login or password',
                    code: 2,
                });
            }
        });

        // if (user.password === password) {
        //     res.send({
        //         login: user.login,
        //         token: ' ---> токен <---',
        //     });
        // } else {
        //     res.status(400).send({
        //         error: 'Incorrect login or password',
        //         code: 2,
        //     });
        // }
    } else {
        res.status(400).send({ error: 'Login not found', code: 1 });
    }
});

router.post('/recover/login', wait(), (req, res) => {
    const { email } = req.body;
    const user = users.find((u) => u.email === email);
    if (user) {
        currentUser = user;
        sendCodeToMail(user.email);

        res.send({ message: 'A code has been sent to your email' });
    } else {
        res.status(400).send({ error: 'Email not found', code: 1 });
    }
});

router.post('/recover/login/confirm', wait(), (req, res) => {
    const { pubkey, code } = req.body;
    const parsedPubKey = forge.pki.publicKeyFromPem(pubkey);

    const login = currentUser.login;
    const buffer = forge.util.createBuffer(login, 'utf8').getBytes();
    const encryptedLogin = parsedPubKey.encrypt(buffer, 'RSA-OAEP');
    const encryptedLoginBase64 = forge.util.encode64(encryptedLogin);

    const answer = {
        login,
    };

    _.set(answer, 'login', encryptedLoginBase64);

    if (codeToSend === Number(code)) {
        res.send({ login: answer.login });
    } else {
        res.status(400).send({ error: 'Incorrect code', code: 2 });
    }
});

router.post('/recover/password', wait(), (req, res) => {
    const { email } = req.body;
    const user = users.find((u) => u.email === email);
    if (user) {
        objIndex = users.findIndex((u) => u.email === email);
        sendCodeToMail(user.email);
        res.send({ message: 'A code has been sent to your email' });
    } else {
        res.status(400).send({ error: 'Email not found', code: 1 });
    }
});

router.post('/recover/password/confirm', wait(), (req, res) => {
    const { code } = req.body;
    if (codeToSend === Number(code)) {
        res.send({ message: 'Success' });
    } else {
        res.status(400).send({ error: 'Incorrect code', code: 2 });
    }
});

router.post('/recover/password/final', wait(), (req, res) => {
    const { password } = req.body;

    hash({ password }, (err, pass, salt, hash) => {
        if (err) throw err;

        users[objIndex].hash = hash;
        users[objIndex].salt = salt;
        objIndex = '';
        res.send({ message: 'Password changed successfully' });
    });

    // users[objIndex].password = password;
    // objIndex = '';
    // res.send({ message: 'Password changed successfully' });
    // res.status(400).send({ error: 'Some error 400' });
});

router.get('/products', (req, res) => {
    res.send(require('./mocks/products/products'));
});

router.post('/order', wait(), (req, res) => {
    const { name, phone, address } = req.body;
    const newOrder = {
        name,
        phone,
        address,
    };
    orders.push(newOrder);
    res.send({
        message: orders.length,
    });
    // res.status(400).send({ error: 'Some error 400' });
});

router.post('/deleteuser', wait(), (req, res) => {
    const { email } = req.body;
    const user = users.find((u) => u.email === email);

    if (user) {
        objIndex = users.findIndex((u) => u.email === email);
        sendCodeToMail(user.email);
        res.send({
            message: 'Deleting. A code has been sent to the entered email',
        });
    } else {
        res.status(400).send({ error: 'Email not foumd', code: 1 });
    }
});

router.post('/deleteuser/confirm', wait(), (req, res) => {
    const { code } = req.body;
    if (codeToSend === Number(code)) {
        users.splice(objIndex, 1);
        objIndex = '';
        res.send({ message: 'Account successfully deleted' });
    } else {
        res.status(400).send({ error: 'Incorrect code', code: 2 });
    }
});

// app.use(router);
// module.exports = app;
module.exports = router;
