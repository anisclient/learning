const pkg = require('./package');
const path = require('path');
// const webpackCopy = require('copy-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//     .BundleAnalyzerPlugin;

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${
                process.env.VERSION || pkg.version
            }/`,
        },
        // plugins: [
        // new webpackCopy({
        //     patterns: [
        // {
        //     from:
        //         './node_modules/react/umd/react.production.min.js',
        //     to: 'externals/react.js',
        //     noErrorOnMissing: true,
        // },
        // {
        //     from:
        //         './node_modules/react-dom/umd/react-dom.production.min.js',
        //     to: 'externals/react-dom.js',
        //     noErrorOnMissing: true,
        // },
        //         {
        //             from: process.env.LOCALES,
        //             to: 'locales',
        //             noErrorOnMissing: true,
        //         },
        //     ],
        // }),
        //     new BundleAnalyzerPlugin({
        //         reportFilename: './temp/stat.html',
        //         analyzerMode: 'static',
        //         defaultSizes: 'gzip',
        //         openAnalyzer: false,
        //     }),
        // ],
        module: {
            rules: [
                {
                    test: /\.css$/i,
                    use: [
                        {
                            loader: 'style-loader',
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    mode: 'local',
                                    exportGlobals: true,
                                    localIdentName: isProd
                                        ? '[hash:base64]'
                                        : '[path]--[name]__[local]--[hash:base64:3]',
                                    localIdentContext: path.resolve(
                                        __dirname,
                                        'src'
                                    ),
                                    exportLocalsConvention: 'camelCase',
                                },
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        // @import "path/to/my.css";
                                        require('postcss-import'),
                                        // for () {}
                                        require('postcss-for'),
                                        // TODO: устарело
                                        require('postcss-simple-vars'),
                                        // :root { --my-var: 0 } ... div { padding: var(--my-var) }
                                        require('postcss-custom-properties')({
                                            // Не оставлять переменную
                                            preserve: false,
                                        }),
                                        // @custom-media --media (min-width: 1281px) ... @media (--media-xl) {}
                                        require('postcss-custom-media')({
                                            // Не оставлять переменную
                                            preserve: false,
                                        }),
                                        // div { div {} }
                                        require('postcss-nested'),
                                        // color(#fff a(90%));
                                        require('postcss-color-function'),
                                        // Лучшее не нуждается в комментариях
                                        require('autoprefixer')(),
                                        // calc(2 * 50px) -> 100px
                                        require('postcss-calc'),
                                        // Удаляем колмментарии из CSS
                                        require('postcss-discard-comments'),
                                        // Минификация css (удаление пустых :root {}, отступов, переносов строк и т.д.)
                                        require('cssnano')({
                                            preset: 'default',
                                        }),
                                    ],
                                },
                            },
                        },
                    ],
                },
            ],
        },
        // externals: {
        //     react: 'react',
        //     'react-dom': 'react-dom',
        //     // 'react-router-dom': 'react-router-dom',
        //     // i18next: 'i18next',
        // },
    },
    navigations: {
        e_zone: '/e_zone',
        'link.e_zone.main': '/e_zone/main',
        'link.e_zone.login': '/e_zone/login',
        'link.e_zone.basket': '/e_zone/basket',
        'link.e_zone.like': '/e_zone/like',
        'link.e_zone.product': '/e_zone/product',
        'link.e_zone.order': '/e_zone/order',
        'link.e_zone.register': '/e_zone/register',
        'link.e_zone.recover.login': '/e_zone/recover/login',
        'link.e_zone.recover.password': '/e_zone/recover/password',
    },
    config: {
        // 'e_zone.api': '/api',
        'e_zone.api': 'https://ezoneserver.herokuapp.com',
    },
};
