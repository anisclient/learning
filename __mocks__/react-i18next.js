const locales = require('../locales/ru.json');

module.exports = {
    useTranslation: () => ({
        t: (key) => locales[key],
        i18n: {
            changeLanguage: () => new Promise(() => {}),
        },
    }),
};
