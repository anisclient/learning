const { navigations, features, config } = require('../ijl.config');

module.exports = {
    getNavigations: () => ({ ...navigations }),
    getFeatures: () => ({ ...features }),
    getConfigValue: (key) => config[key],
};
